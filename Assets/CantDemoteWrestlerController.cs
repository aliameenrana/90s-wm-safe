﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CantDemoteWrestlerController : MonoBehaviour {

    public GameObject OKButton;

    private void OnEnable()
    {
        OKButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.CloseCantDemoteChamp());
    }
}
