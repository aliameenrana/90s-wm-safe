﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromotionConfirmController : MonoBehaviour {

    public GameObject PromotionConfirmYes, PromotionConfirmNo;

    private void OnEnable()
    {
        PromotionConfirmYes.GetComponent<Button>().onClick.RemoveAllListeners();
        PromotionConfirmNo.GetComponent<Button>().onClick.RemoveAllListeners();
        PromotionConfirmYes.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.ConfirmPromoteWrestlerStatus());
        PromotionConfirmNo.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.ClosePromoteWrestlerStatus());
    }

}
