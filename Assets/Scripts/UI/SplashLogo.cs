﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashLogo : MonoBehaviour {
	public float fadeTime = 5f;
	public Animator titleScreen, loginScreen;
	
	private float timePassed;

	public GameObject blackBackground;

	public bool isSecondScreen;
//	public GameSetupItems gsi;

	void Start()
	{
		if (blackBackground != null)
			blackBackground.SetActive(true);
	}

	void Update() {
		timePassed += Time.deltaTime;

		if (timePassed > fadeTime)
			ChangeScreen();
	}

	public void ChangeScreen() {
		if (ScreenHandler.current != null && titleScreen != null && loginScreen != null)
		{
			if(isSecondScreen && PlayerPrefs.GetString("UserID", "") == "")
			{
				Debug.Log("Umar:: Force Login.");
				ScreenHandler.current.OpenPanel(loginScreen);
			}
			else
			{
				ScreenHandler.current.OpenPanel(titleScreen);
			}

			if (blackBackground != null)
				blackBackground.SetActive(false);
		}
	}

	public void OnDisable(){
		if(!isSecondScreen)
			GameObject.Find("UIManager").GetComponent<GameSetupItems>().PlayTheme();
	}
}
