﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleScreen : MonoBehaviour {
	public GameObject newGameConfirmWindow;
	public GameObject newGamePlusConfirmWindow;
	public string newGamePlusText = "New Game+!";
	public UIManagerScript UIManager;

	public void OnEnable(){
		//Debug.LogWarning (HasSave ());
		if (HasSave()) {
			GameObject myButton;
			
			myButton = GameObject.Find ("ContinueButton");
			myButton.GetComponent<Button>().interactable = true;
		} else {
			GameObject myButton;
			
			myButton = GameObject.Find ("ContinueButton");
			myButton.GetComponent<Button>().interactable = false;
		}

		if (NewGamePlus()) {
			GameObject myButton;
			
			myButton = GameObject.Find ("NewGameButton");
			myButton.GetComponentInChildren<Text>().text = newGamePlusText;
		}
	}

	public void OpenNewGame() {
		if (NewGamePlus()) {
			newGamePlusConfirmWindow.SetActive(true);
		} else if (HasSave()) {
			newGameConfirmWindow.SetActive(true);
		} else {
			UIManager.NewGame();
		}
	}

	bool HasSave()
	{
		return (PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE") != "NONE");
	}

	bool NewGamePlus() {
		return (GamePlaySession.instance != null && (!GamePlaySession.instance.inTutorialMode || GamePlaySession.instance.newGamePlus));
	}
}
