﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TransitionScreen : MonoBehaviour {
	public float fadeTime = 5.5f;
	public Animator resultsScreen;

	public TransitionSegmentDisplay segmentDisplay;

	public PopUpMessage transitionPopUpMessage;
	
	[HideInInspector]
	public bool needSave;
	
	[HideInInspector]
	public ShowObject show;

	private bool showingSegment;
	private int segmentNum;
	
	public void OpenScreen() {
		if (!GamePlaySession.instance.transitionMessageClear && !GamePlaySession.instance.newGamePlus) {
			PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

			if (popUpWindow != null) {
				popUpWindow.OpenPopUpWindow(new PopUpMessage[] {transitionPopUpMessage}, this.gameObject.GetComponent<Animator> ());
			}

			GamePlaySession.instance.transitionMessageClear = true;
		} else {
			if (ScreenHandler.current != null)
				ScreenHandler.current.OpenPanel (this.gameObject.GetComponent<Animator> ());
		}

		/*
		if (GameObject.Find("UIManager").GetComponent<UIManagerScript>().sfxEnabled)
			audio.Play();
		*/

		ResetDisplay();
	}
	
	private void ResetDisplay() {
		segmentNum = 0;
		showingSegment = false;
		segmentDisplay.ResetSegment();
		segmentDisplay.needSave = needSave;
	}
	
	void Update() {
		if (!showingSegment) {
			segmentDisplay.ProcessSegment(show.segments[segmentNum]);
			segmentDisplay.ShowSegment();
			showingSegment = true;
		}
		
		if (showingSegment) {
			if (segmentDisplay.SegmentDone()) {
				segmentNum += 1;
				if (segmentNum >= show.segments.Length) {
					ChangeScreen();
				} else {
					segmentDisplay.ResetSegment();
					showingSegment = false;
				}
			}
		}

		/*
		// If we need to save and we're mid-transition then do so
		if (timePassed >= (fadeTime / 4f) && needSave) {
			GamePlaySession.instance.SaveAll();
			needSave = false;
		}

		if (timePassed > fadeTime)
			ChangeScreen();
		*/
	}
	
	public void ChangeScreen() {
		if (ScreenHandler.current != null && resultsScreen != null)
			ScreenHandler.current.OpenPanel(resultsScreen);
	}
}
