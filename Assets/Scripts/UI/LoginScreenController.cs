﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Text.RegularExpressions;

public class LoginScreenController : MonoBehaviour
{
	public InputField emailTF, passwordTF;
	public Text message;
	public GameObject blackBG;
	public GameObject socialManager;
    public GameObject UIManager;
    public GameObject ServerManager;
	bool serverWait = false;
	FaceBookManager FBManager;

	/// <summary>
	/// Regular expression, which is used to validate an E-Mail address.
	/// </summary>
	const string MatchEmailPattern = 
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

	/// <summary>
	/// Checks whether the given Email-Parameter is a valid E-Mail address.
	/// </summary>
	/// <param name="email">Parameter-string that contains an E-Mail address.</param>
	/// <returns>True, when Parameter-string is not null and 
	/// contains a valid E-Mail address;
	/// otherwise false.</returns>
	bool IsEmail(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}

	void Awake()
	{
		FBManager = socialManager.GetComponent<FaceBookManager>();
        UIManager = GameObject.Find("UIManager");
        ServerManager = GameObject.Find("ServerManager");
	}

	void OnEnable()
	{
		blackBG.SetActive(true);
       
		FBManager.loginCallbackEvent += FBLoginCallback;
	}

	void OnDisable()
	{
		blackBG.SetActive(false);
        
        FBManager.loginCallbackEvent -= FBLoginCallback;
	}

	void ShowToast(string text)
	{
		StartCoroutine(ShowToastCR(text));
	}

	IEnumerator ShowToastCR(string text)
	{
		message.gameObject.SetActive(true);
		message.text = text;
		yield return new WaitForSeconds(3f);
		message.gameObject.SetActive(false);
	}

    public void GameLogin()
    {
		if(!string.IsNullOrEmpty(emailTF.text) && !string.IsNullOrEmpty(passwordTF.text))
		{
			if(!IsEmail(emailTF.text))
			{
				ShowToast("Please enter valid email address");
			}
			else if(passwordTF.text.Length < 8)
			{
				ShowToast("Password should be atleast 8 characters long.");
			}
			else
			{
				StartCoroutine(SendRequest("LOGIN"));
				//Debug.Log("Sending login request via email");
			}
		}
		else
		{
			ShowToast("Please Enter Email & Password");
		}
    }

    public void FacebookLogin()
    {
		if(!FBManager.IsFBLoggedIn())
		{
			FBManager.FBLogIn();
			//Debug.Log("Sending login request to FB");
		}
    }
    
	public void ForgotPassword()
    {
		if(!string.IsNullOrEmpty(emailTF.text) && IsEmail(emailTF.text))
		{
			StartCoroutine(SendRequest("FORGOT"));
			Debug.Log("Sending forgot request to server");
		}
		else if(!IsEmail(emailTF.text))
		{
			ShowToast("Please enter valid email address");
		}
    }

	void FBLoginCallback(bool result)
	{
		if(result)
		{
			StartCoroutine(SendRequest("LOGIN"));
			Debug.Log("Sending login request via FB token");
		}
	}

	private IEnumerator SendRequest(string reqType)
    {
		string url = "http://bugdevstudios.com/wrestling/server.php?REQUEST="+reqType+"&ACCESS_TOKEN="+FBManager.GetFBAccessToken()+"&EMAIL="+emailTF.text+"&PASSWORD="+passwordTF.text;
        //Debug.Log(url);
        
		WWW www = new WWW(url);

        yield return www;

        if (www.error != null)
        {
            //Debug.Log(www.error);
            yield break;
        }

		//Debug.Log(www.text);
		ProcessResponse(www.text);

        www.Dispose();
    }

	void ProcessResponse(string response)
	{
		var N = JSON.Parse(response);
//		var statusString = N["STATUS"].Value;
		var responseCode = N["CODE"].AsInt;
		var userID = N["USER_ID"].Value;

        Debug.Log("Response Code: " + responseCode);

		if(responseCode>0)
		{
			if(responseCode == 1)//1=new user
			{
				//Debug.Log("Server code : " + responseCode.ToString() + "Logging on PF with ID : " + userID);
				PlayerPrefs.SetString("UserID", userID);
				PlayFabManager.GetInstance().Login(userID);
				StartCoroutine("PFLoginWait");
			}
			else if(responseCode == 2)//2=existing user
			{
				serverWait = true;
				StartCoroutine("ServerWait");
				//Debug.Log("Server code : " + responseCode.ToString() + " Logging on PF with ID : " + userID);
				PlayerPrefs.SetString("UserID", userID);
				ServerManager.GetComponent<DataHandler>().GetSessionDataFromServer(UpdateExistingUserData);
				PlayFabManager.GetInstance().Login(userID);
                StartCoroutine("PFLoginWait");
            }
			else if(responseCode == 3)
			{
				ShowToast("New password sent to email address.");
			}
		}
		else
		{
			if(responseCode == -2)
			{
				ShowToast("An error occured while sending email for password reset. Please try again.");
			}
			else if(responseCode == -3)
			{
				ShowToast("No record found of given email address.");
			}
		}
	}

	IEnumerator PFLoginWait()
	{
		while(!PlayFabManager.GetInstance().IsPFLoggedIn())
			yield return new WaitForSeconds(0.5f);
		
		Debug.Log("Logged in on PF");

		UIManager.GetComponent<UIManagerScript>().GoToTitle();
	}

	IEnumerator ServerWait()
	{
		while(!PlayFabManager.GetInstance().IsPFLoggedIn())
			yield return new WaitForSeconds(0.5f);

		while(serverWait)
			yield return new WaitForSeconds(0.5f);

		//Debug.Log("Logged in on PF and got data from server");
		UIManager.GetComponent<UIManagerScript>().GoToTitle();
	}

	public void UpdateExistingUserData(bool result)
	{
		serverWait = !result;
	}

    public void OpenRegistrationScreen()
    {
        if (ScreenHandler.current != null && UIManager.GetComponent<UIManagerScript>().RegisterScreen != null)
            ScreenHandler.current.OpenPanel(UIManager.GetComponent<UIManagerScript>().RegisterScreen);
    }
}
