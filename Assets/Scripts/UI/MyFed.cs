﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MyFed : MonoBehaviour {
	
	public Text FedNameText;
	public Text FedBonusText;
	public Image FedImage;
	
	public Text CommishNameText;
	public Text CommishBonusText;
	public Image CommishImage;

	public Text Tag1ChampText;
	public Text Tag2ChampText;
	public Text ContinentalChampText;
	public Text WorldChampText;
	public Text Tag1LastChampText;
	public Text Tag2LastChampText;
	public Text ContinentalLastChampText;
	public Text WorldLastChampText;

	public GameObject walletPanel;

	public Image ratingsImage;

	public CardSelectScreenScript cardViewer;

	public PopUpMessage newWrestlersMessage;

	private GameObject UIManager;
	private GameObject playerHUD;

	private GameSetupItems setupItems;

	public void OpenMyFedPanel() {
		UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		setupItems = UIManager.GetComponent<GameSetupItems>();

        if (playerHUD != null)
			playerHUD.SetActive(false);

		CheckMyFedData();

		if (ScreenHandler.current != null)
			ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());

		walletPanel.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
		walletPanel.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
	}

	public void CloseFedPanel() {
		UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;

		bool hasCards = false;
		NewCardsObject newCards = new NewCardsObject();

		if (GamePlaySession.instance.inTutorialMode && GamePlaySession.instance.GiveInitialCards() && !GamePlaySession.instance.newGamePlus) {
			if (playerHUD != null)
				playerHUD.SetActive(false);

			newCards.message = newWrestlersMessage;
			if(GamePlaySession.instance.CheckCard(153) == null){
				newCards.cards.Add(setupItems.wrestlers.cards[153].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[129].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[21].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[119].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[79].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[73].Clone());  //tag champ
				newCards.cards.Add(setupItems.wrestlers.cards[25].Clone());  //tag champ
				newCards.cards.Add(setupItems.wrestlers.cards[123].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[131].Clone());
				newCards.cards.Add(setupItems.wrestlers.cards[157].Clone());
                newCards.cards.Add(setupItems.wrestlers.cards[406].Clone());
                newCards.cards.Add(setupItems.wrestlers.cards[151].Clone());  //continental champ
				newCards.cards.Add(setupItems.wrestlers.cards[45].Clone());  //world champ
				newCards.cards.Add(setupItems.venues.cards[178].Clone());
				newCards.cards.Add(setupItems.venues.cards[177].Clone());  
				newCards.cards.Add(setupItems.venues.cards[179].Clone());  
				newCards.cards.Add(setupItems.matches.cards[189].Clone());  // wrestling match
			}

			hasCards = true;
		}

		if (hasCards) {
			if (cardViewer != null)
				cardViewer.OpenCardView(newCards);
		} else {
			UIManager.GetComponent<UIManagerScript>().GoToCalendar(false);
		}
		
		// Set current championship belt holders if they've not yet been set
		if (GamePlaySession.instance.titleChampion == null) {
			GamePlaySession.instance.titleChampion = (WrestlerCardObject) GamePlaySession.instance.CheckCard(151);
			GamePlaySession.instance.worldChampion = (WrestlerCardObject) GamePlaySession.instance.CheckCard(45);
			GamePlaySession.instance.tagChampions = new WrestlerCardObject[]{ (WrestlerCardObject) GamePlaySession.instance.CheckCard(73),
				(WrestlerCardObject) GamePlaySession.instance.CheckCard(25)};

            GamePlaySession.instance.titleChampion.CrowningDate = "Default";
            GamePlaySession.instance.worldChampion.CrowningDate = "Default";
            GamePlaySession.instance.tagChampions[0].CrowningDate = "Default";
            GamePlaySession.instance.tagChampions[1].CrowningDate = "Default";

            GamePlaySession.instance.titleChampion.Defenses = 0;
            GamePlaySession.instance.worldChampion.Defenses = 0;
            GamePlaySession.instance.tagChampions[0].Defenses = 0;
            GamePlaySession.instance.tagChampions[1].Defenses = 0;

            GamePlaySession.instance.AllWorldChampions.Add((WrestlerCardObject)GamePlaySession.instance.CheckCard(45));
            GamePlaySession.instance.AllTitleChampions.Add((WrestlerCardObject)GamePlaySession.instance.CheckCard(151));
            GamePlaySession.instance.AllTagChampions.Add((WrestlerCardObject)GamePlaySession.instance.CheckCard(73));
            GamePlaySession.instance.AllTagChampions.Add((WrestlerCardObject)GamePlaySession.instance.CheckCard(25));
        }
	}

	public void CheckMyFedData() {
		if(walletPanel.activeSelf){
            try
            {
                walletPanel.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
                walletPanel.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
            }
            catch
            {
                Debug.Log("ShowMyCash & ShowMyTokens has no receiver...");
            }
		}
		ratingsImage.fillAmount = GamePlaySession.instance.averageRating;

		//Debug.Log("Federation num: "+GamePlaySession.instance.selectedFederation);
		FedNameText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.selectedFederation].NameString;
		FedBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.selectedFederation].BonusString;
		FedImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.selectedFederation].littleSprite;
		
		CommishNameText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.selectedComissioner].NameString;
		CommishBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.selectedComissioner].BonusString;
		CommishImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.selectedComissioner].littleSprite;

		if (ratingsImage != null)
			ratingsImage.fillAmount = GamePlaySession.instance.GetAverageStarRating();

		string wcn;
		string ccn;
		string[] ttns = new string[2];

		if(GamePlaySession.instance.tagChampions == null){
			ttns[0] = "Jesse Wild";
			ttns[1] = "Chet Skye";
		}
		else{
			try{
				ttns[0] = GamePlaySession.instance.tagChampions[0].name;
				ttns[1] = GamePlaySession.instance.tagChampions[1].name;
			}
			catch(System.Exception e){
				ttns[0] = "Jesse Wild";
				ttns[1] = "Chet Skye";
			}
		}

		if(GamePlaySession.instance.titleChampion == null){
			ccn = "Star Boy";
		}
		else{
			ccn = GamePlaySession.instance.titleChampion.name;
		}

		if(GamePlaySession.instance.worldChampion == null){
			wcn = "Dr. B";
		}
		else{
			wcn = GamePlaySession.instance.worldChampion.name;
		}

		Tag1ChampText.text = ttns[0];
		Tag2ChampText.text = ttns[1];
		ContinentalChampText.text = ccn;
		WorldChampText.text = wcn;

		string wcnl;
		string ccnl;
		string[] ttnsl = new string[2];

		ttnsl[0] = "";
		ttnsl[1] = "";
	
		 if(GamePlaySession.instance.tagLastChampions != null){
			if(GamePlaySession.instance.tagLastChampions[0] != null){
				ttnsl[0] = GamePlaySession.instance.tagLastChampions[0].name;
				ttnsl[1] = GamePlaySession.instance.tagLastChampions[1].name;
			}
		}
		
		if(GamePlaySession.instance.titleLastChampion == null){
			ccnl = "";
		}
		else{
			ccnl = GamePlaySession.instance.titleLastChampion.name;
		}
		
		if(GamePlaySession.instance.worldLastChampion == null){
			wcnl = "";
		}
		else{
			wcnl = GamePlaySession.instance.worldLastChampion.name;
		}
		
		Tag1LastChampText.text = ttnsl[0];
		Tag2LastChampText.text = ttnsl[1];
		ContinentalLastChampText.text = ccnl;
		WorldLastChampText.text = wcnl;
	}
}
