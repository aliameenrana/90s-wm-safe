﻿using UnityEngine;
using System.Collections;

public class EndGameScreen : MonoBehaviour {
	public float fadeTime = 5f;
	public Animator titleScreen;

	public PopUpMessage endGameMessage;

	public GameObject UIManager;
	
	private float timePassed;
	
	void Update() {
		timePassed += Time.deltaTime;
		
		if (timePassed > fadeTime)
			ChangeScreen();
	}

	public void OpenScreen() {
		timePassed = 0f;

		if (ScreenHandler.current != null) {
			Animator endGameScreen = this.GetComponent<Animator>();
			ScreenHandler.current.OpenPanel(endGameScreen);
		}
	}
	
	public void ChangeScreen() {
		PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
		
		if (popUpWindow != null && titleScreen != null) {
			popUpWindow.OpenPopUpWindow(new PopUpMessage[] {endGameMessage}, titleScreen);
		}
	}
}
