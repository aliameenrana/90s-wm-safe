﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemotionConfirmController : MonoBehaviour {

    public GameObject DemotionConfirmYes, DemotionConfirmNo;

    private void OnEnable()
    {
        DemotionConfirmYes.GetComponent<Button>().onClick.RemoveAllListeners();
        DemotionConfirmNo.GetComponent<Button>().onClick.RemoveAllListeners();
        DemotionConfirmYes.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.ConfirmDemoteWrestlerStatus());
        DemotionConfirmNo.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.CloseDemoteWrestlerStatus());
    }

}
