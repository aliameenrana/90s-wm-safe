﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonSound : MonoBehaviour {
	private UIManagerScript UIManager;

	void Start() {
		UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();

		Button button = this.gameObject.GetComponent<Button>();
		button.onClick.AddListener(() => ClickSound());
	}

	void ClickSound() {
		if (UIManager.sfxEnabled && UIManager.buttonSoundSource != null) {
			UIManager.buttonSoundSource.Play();
		}
	}
}
