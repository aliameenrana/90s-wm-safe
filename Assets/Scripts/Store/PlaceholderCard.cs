﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlaceholderCard : MonoBehaviour {
	public string commonMessage;
	public string uncommonMessage;
	public string rareMessage;
	public Text messageText;
}
