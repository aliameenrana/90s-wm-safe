﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;

public class DataHandler : MonoBehaviour
{
	public string serverURL = "http://bugdevstudios.com/wrestling/server.php";
    public GameObject UIManager;
    public static DataHandler instance = null;
	string data = "";
	Action<bool> callback;

    private void Awake()
    {
        instance = this;
    }

	IEnumerator Start()
	{
		while(PlayerPrefs.GetString("UserID", "") == "")
			yield return new WaitForSeconds(0.5f);

		InvokeRepeating("ScheduleUpload", 60f, 60f);

//		yield return new WaitForSeconds(15f);
//		yield return StartCoroutine("UploadData");
//		yield return new WaitForSeconds(15f);
//		yield return StartCoroutine("InvokeServer");
//		GamePlaySession.instance = GamePlaySession.Load(data);
//		Debug.Log("Process Done");

	}

	void ScheduleUpload()
	{
		StartCoroutine("UploadData");
	}

	IEnumerator UploadData()
	{
		string userId = PlayerPrefs.GetString("UserID");
		string time = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
		string data = UIManagerScript.instance.myStr;

		WWWForm form = new WWWForm();
		form.AddField("REQUEST", "UPDATE_GPS");
		form.AddField("USER_ID", userId);
		form.AddField("TIME", time);
		form.AddField("GPS", data);

		Debug.Log("UserID : " + userId + " Time : " + time);
//		Debug.Log(data);

		WWW www = new WWW(serverURL, form);

		yield return www;

		if (www.error != null)
		{
			Debug.Log(www.error);
			yield break;
		}

		//Debug.Log("Umar:: game Session sent to server");
		Debug.Log(www.text);
		www.Dispose();
	}

	public void GetSessionDataFromServer(Action<bool> OnDataRecieved)
	{
		callback = OnDataRecieved;
		StartCoroutine("InvokeServer");
	}

	IEnumerator InvokeServer()
	{
		string url = serverURL + "?REQUEST=GET_GPS&USER_ID=" + PlayerPrefs.GetString("UserID");
		Debug.Log(url);
		WWW www = new WWW(url);

		yield return www;

		if (www.error != null)
		{
			Debug.Log(www.error);
			yield break;
		}

//		Debug.Log(www.text);
		ProcessResponse (www.text);
		www.Dispose();
	}

	void ProcessResponse(string response)
	{
		var N = JSON.Parse(response);
		var responseStatus = N["CODE"].AsInt;
		data = N["GPS"].Value;
		var lastUpdate = N["GPS_UPDATED"].Value;
		var status = N["STATUS"].Value;
        UIManager = GameObject.Find("UIManager");
		if(responseStatus == 6)
		{
			Debug.Log("Data recieved successfully...");

            GamePlaySession.instance = GamePlaySession.Load(data);
            GamePlaySession.instance.SaveAll();

            GamePlaySession.instance.GSI.GetComponent<UIManagerScript>().PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
            GamePlaySession.instance.GSI.GetComponent<UIManagerScript>().PlayerHUD.GetComponent<HUDScript>().SetFedImage();

            foreach (CardObject item in GamePlaySession.instance.myCards)
            {
                item.PostSerialize();
            }
            
			if(callback != null)
				callback(true);
		}
		else
		{
			if(callback != null)
				callback(false);
			Debug.Log("Error Retrieving Data from server.");
		}
	}
}