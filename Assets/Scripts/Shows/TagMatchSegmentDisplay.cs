﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TagMatchSegmentDisplay : SegmentDisplay {
	
	public Text matchTitle;
	public Text wrestlerName1;
	public Text wrestlerName2;
	public Text wrestlerName3;
	public Text wrestlerName4;
	public Image wrestlerHeadshot1;
	public Image wrestlerHeadshot2;
	public Image wrestlerHeadshot3;
	public Image wrestlerHeadshot4;
	
	public override void OpenSegmentSetup() {
		base.OpenSegmentSetup();

		UIManager = GameObject.Find("UIManager");
		Animator tagMatchScreen = UIManager.GetComponent<UIManagerScript>().TagMatchSetupScreen;
		TagMatchSegmentSetup tagMatchSetup = tagMatchScreen.gameObject.GetComponent<TagMatchSegmentSetup>();
		
		if (ScreenHandler.current != null && tagMatchScreen != null)
			ScreenHandler.current.OpenPanel(tagMatchScreen);
		
		tagMatchSetup.SetData(this);
	}
	
	public override void UpdateSegmentData() {
		SegmentObject segment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];
		
		if (segment != null) {
			if (segment.isTitleMatch && setupButton != null && setupButtonText != null) {
				ColorBlock titleColorBlock = setupButton.colors;
				titleColorBlock.normalColor = titleSegmentNormalColor;
				titleColorBlock.highlightedColor = titleSegmentHighlightColor;
				titleColorBlock.pressedColor = titleSegmentPressedColor;

				switch (segment.titleMatchType) {
				case SegmentObject.TitleMatchType.TAG:
					setupButton.colors = titleColorBlock;
					setupButtonText.text = "+Tag Title Match!";
					break;
				}
			}
			if (segment.cards != null) {
				if (matchTitle != null) {
					if (segment.isTitleMatch) {
						switch (segment.titleMatchType) {
						case SegmentObject.TitleMatchType.TAG:
							matchTitle.text = "Tag Title Match";
							break;
						}
					} else {
						matchTitle.text = "Tag Match";
					}
				}

				CardObject wrestlerCard1 = segment.cards[2];
				CardObject wrestlerCard2 = segment.cards[3];
				CardObject wrestlerCard3 = segment.cards[5];
				CardObject wrestlerCard4 = segment.cards[6];
				
				if (wrestlerCard1 != null) {
					wrestlerName1.text = wrestlerCard1.name;
					wrestlerHeadshot1.sprite = GamePlaySession.instance.CheckCard(wrestlerCard1.CardNumber).headshotImage;
				}
				if (wrestlerCard2 != null) {
					wrestlerName2.text = wrestlerCard2.name;
					wrestlerHeadshot2.sprite = GamePlaySession.instance.CheckCard(wrestlerCard2.CardNumber).headshotImage;
				}
				if (wrestlerCard3 != null) {
					wrestlerName3.text = wrestlerCard3.name;
					wrestlerHeadshot3.sprite = GamePlaySession.instance.CheckCard(wrestlerCard3.CardNumber).headshotImage;
				}
				if (wrestlerCard4 != null) {
					wrestlerName4.text = wrestlerCard4.name;
					wrestlerHeadshot4.sprite = GamePlaySession.instance.CheckCard(wrestlerCard4.CardNumber).headshotImage;
				}
			}
		}
	}
}
