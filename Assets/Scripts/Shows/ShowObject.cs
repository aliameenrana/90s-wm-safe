﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;

[System.Serializable]
public class ShowObject {
	[XmlElement("ShowName")]
	public string showName;
	[XmlElement("ShowTitle")]
	public string displayTitle;

	[XmlElement("VenueID")]
	public int venueCard = 0;
	[XmlElement("HasResults")]
	public bool showResults = true; // Should this show type show results (used for Now You Know slots, etc.)
	[XmlElement("IsP4V")]
	public bool isP4V = false;

	[XmlArray("Segments"), XmlArrayItem("Segment")]
	public SegmentObject[] segments;
	
	[XmlElement("ShowPopUp")]
	public PopUpMessage showPopUp;
	public bool popUpClear;

	[HideInInspector]
	[XmlElement("ShowFinished")]
	public bool showFinished = false; // A bool to determine if this show was already run through the results process

	[HideInInspector]
	[XmlElement("ShowAttendance")]
	public Attendance showAttendance;

	[HideInInspector]
	[XmlElement("ShowEarnings")]
	public int showEarnings;

	[HideInInspector]
	[XmlElement("ShowRating")]
	public float showRating;

	[HideInInspector]
	[XmlElement("BlurbNumber")]
	public int blurbNumber;

    public static ShowObject instance;
    public int CurrentWeek;

    public SegmentObject ThisSegment;
    public bool AwardShow;


    void Awake()
    {
        instance = this;
    }

    public ShowObject(){

	}
	public ShowObject Clone() {
		ShowObject newShow = new ShowObject();
		newShow.showName = this.showName;
		newShow.displayTitle = this.displayTitle;
		newShow.venueCard = this.venueCard;
		newShow.showResults = this.showResults;
		newShow.showAttendance = this.showAttendance;
		newShow.isP4V = this.isP4V;
		newShow.showPopUp = this.showPopUp;
		newShow.popUpClear = this.popUpClear;
		newShow.segments = new SegmentObject[this.segments.Length];
		for (int i = 0;i < this.segments.Length;i++) {
			newShow.segments[i] = new SegmentObject();
			newShow.segments[i].segmentType = this.segments[i].segmentType;
			newShow.segments[i].isTitleMatch = this.segments[i].isTitleMatch;
			newShow.segments[i].titleMatchType = this.segments[i].titleMatchType;
			newShow.segments[i].isFixed = this.segments[i].isFixed;
			newShow.segments[i].cardIDs = this.segments[i].cardIDs;
			newShow.segments[i].availableCardIDs = this.segments[i].availableCardIDs;
			newShow.segments[i].lockedOptions = this.segments[i].lockedOptions;
			newShow.segments[i].popupMessage = this.segments[i].popupMessage;
			newShow.segments[i].popUpClear = this.segments[i].popUpClear;
		}
        newShow.AwardShow = false;
		return newShow;
	}
	
	public float GetStarRating() {
		if (showRating >= 19f) {
			// 5 stars
			return 1f;
		} else if (showRating >= 15f) {
			// 4 stars
			return 0.8f;
		} else if (showRating >= 11f) {
			// 3 stars
			return 0.6f;
		} else if (showRating >= 7f) {
			// 2 stars
			return 0.4f;
		} else if (showRating >= 3f) {
			// 1 star
			return 0.2f;
		} else {
			// 0 stars
			return 0f;
		}
	}

	public void GenerateShowResults() {
		// If we already finished this show we don't need to regenerate these values
		if (showFinished)
			return;

		// Generate attendance result for the show
		VenueCardObject venue = (VenueCardObject)GamePlaySession.instance.CheckCard(venueCard);
		if (venue != null)
			showAttendance = venue.GetAttendanceResults();

		// Calculate overall show rating and generate results for each segment
		// Also grabs any cash bonuses from each segment based on sponsors
		showRating = 0f;
		float numberOfRatings = 0f;
		int bonusCash = 0;
		for (int i = 0;i < segments.Length;i++) {
            // Generate segment results

            SegmentObject segment = segments[i];
            segment.GetResults(segment);

            // Add segment rating to our average
            showRating += segment.segmentRating;
			numberOfRatings += 1f;

			// Check if segment had a sponsor
			foreach (CardObject card in segment.cards) {
                    if (card != null) {
					if (card.myCardType == CardType.SPONSOR) {
						FlavorCardObject flavor = (FlavorCardObject) card;
						bonusCash += flavor.bonuses.Cash;
					} else if (card.myCardType == CardType.MERCH) {
						FlavorCardObject flavor = (FlavorCardObject) card;
						bonusCash += flavor.bonuses.Cash;
					}
				}
			}
		}
		showRating = showRating / numberOfRatings;

		// Check if we get a commissioner bonus
		bonusCash += GamePlaySession.instance.GetCommishBonus();
		Debug.Log("Bonus Cash Awarded = " + bonusCash.ToString());
        //ALI AMEEN:: TESTING CHAMPIONS
        //foreach (WrestlerCardObject item in GamePlaySession.instance.AllWorldChampions)
        //{
        //    Debug.Log("WORLD CHAMPION: " + item.name);
        //}
        //foreach (WrestlerCardObject item in GamePlaySession.instance.AllTitleChampions)
        //{
        //    Debug.Log("TITLE CHAMPION: " + item.name);
        //}
        //foreach (WrestlerCardObject item in GamePlaySession.instance.AllTagChampions)
        //{
        //    Debug.Log("TAG CHAMPION: " + item.name);
        //}
        // Get our earnings multiplier
        float earningsMult = 0f;
		float starValue = GetStarRating();
		if (starValue == 0.2f) {
			// 1 star
			earningsMult = 0.5f;
		} else if (starValue == 0.4f) {
			// 2 stars
			earningsMult = 0.75f;
		} else if (starValue == 0.6f) {
			// 3 stars
			earningsMult = 1f;
		} else if (starValue == 0.8f) {
			// 4 stars
			earningsMult = 1.5f;
		} else if (starValue == 1f) {
			// 5 stars
			earningsMult = 2f;
		}

		// Now calculate our total show earnings
		showEarnings = Mathf.FloorToInt( (showAttendance.cashEarnedGross * earningsMult) + bonusCash );

		showFinished = true;
	}
	public void PostDeserialize(){
		foreach(SegmentObject seg in segments){
			seg.PostDeserialize();
		}

	}
}

[System.Serializable]
public enum BonusType {
	Flavor,
	Wrestler,
	GoodBadMatch,
	MatchFlavor
}

[System.Serializable]
public class ResultBonus {
	public BonusType type;
	public bool favGood;
	public int wrestlerNum;
	public int cardNumber;
	public float value;
}

[System.Serializable]
public class SegmentObject: ISerializationCallbackReceiver {	

	[System.Serializable]
	public enum SegmentType {
		MATCH,
		TAG_MATCH,
		MIC_SPOT,
		SKIT
	}

	[System.Serializable]
	public enum TitleMatchType {
		TAG,
		CONTINENTAL,
		WORLD
	}

	[XmlElement("SegmentType")]
	public SegmentType segmentType;
	
	[XmlElement("SegmentTitle")]
	public string segmentTitle;

	[XmlElement("IsTitleMatch")]
	public bool isTitleMatch; // Is this segment a title match?
	[XmlElement("TitleMatchType")]
	public TitleMatchType titleMatchType; // If we're a title match, this will dictate the type of title
	[XmlElement("IsFixed")]
	public bool isFixed; // Is this segment fixed and uneditable by the player?
	[XmlArray("CardIDs"), XmlArrayItem("CardID")]
	public int[] cardIDs; // The list of IDs of cards to use in this show
	[XmlArray("AvailableCardIDs"), XmlArrayItem("CardID")]
	public int[] availableCardIDs; // A list of card IDs if this segment's setup should be limited to a specific set of cards
	[XmlArray("LockedOptions"), XmlArrayItem("OptionID")]
	public int[] lockedOptions; // Set IDs for any options which should be unable to be set in this segment

	[XmlElement("PopupMessage")]
	public PopUpMessage popupMessage;
	public bool popUpClear;

	[HideInInspector]
	[System.NonSerialized] [XmlIgnore] 
	public CardObject[] cards; // The cards to be used in this segment's calculations

	[HideInInspector]
	[System.NonSerialized] [XmlIgnore] 
	public WrestlerCardObject[] team1;

	[XmlArray("Team1", IsNullable = true), XmlArrayItem("Team1ID")]
	public int[] team1IDs;

	[HideInInspector]
	[System.NonSerialized] [XmlIgnore] 
	public WrestlerCardObject[] team2;

	[XmlArray("Team2", IsNullable = true), XmlArrayItem("Team2ID")]
	public int[] team2IDs;

	[HideInInspector]
	[System.NonSerialized] [XmlIgnore] 
	public WrestlerCardObject[] favorites;

	[XmlArray("Favorites", IsNullable = true), XmlArrayItem("FavoriteID")]
	public int[] favoriteIDs;

	[HideInInspector]
	[System.NonSerialized] [XmlIgnore] 
	public WrestlerCardObject[] underdogs;

	[XmlArray("Underdogs", IsNullable = true), XmlArrayItem("UnderdogID")]
	public int[] underdogIDs;

	[HideInInspector]
	[XmlElement("MatchTarget")]
	public int matchTarget;

	[HideInInspector]
	[System.NonSerialized] [XmlIgnore] 
	public WrestlerCardObject[] winners;

	[XmlArray("Winners", IsNullable = true), XmlArrayItem("WinnerID")]
	public int[] winnerIDs;


	[HideInInspector]
	[XmlElement("SegmentRating")]
	public float segmentRating;

	[HideInInspector]
	[XmlElement("SegmentPassed")]
	public bool segmentPassed;

	[HideInInspector]
	[XmlElement("SegmentWrestlerRatings")]
	public int[] segmentWrestlerRatings;

	[HideInInspector]
	[XmlArray("Team1SkillChanges", IsNullable = true), XmlArrayItem("SkillChange")]
	public int[] team1SkillChange;
	[HideInInspector]
	[XmlArray("Team2SkillChanges", IsNullable = true), XmlArrayItem("SkillChange")]
	public int[] team2SkillChange;

	[XmlElement("PendingSetup")]
	public bool isPendingSetup = true; // Is this segment still waiting to be set up by the player?

	[HideInInspector]
	[XmlElement("SegmentFinished")]
	public bool segmentFinished;

	[HideInInspector]
	[XmlArray("SegmentBonuses", IsNullable = true), XmlArrayItem("Bonus")]
	public List<ResultBonus> segmentBonuses = new List<ResultBonus>();

    [HideInInspector]
    [XmlElement("DisQualification")]
    public int DisQualify=0;

    public SegmentObject() {

	}

	// Processes the Card IDs set in the Unity Inspector into
	// card objects for use on the segment setup screen
	public void ProcessCardIDs() {
		GameSetupItems items = GameObject.Find("UIManager").GetComponent<GameSetupItems>();

		if (cards == null) {
			int numberOfCards = cardIDs.Length;

			// Check how many cards we should be storing based on segment type
			switch (segmentType) {
			case SegmentType.MATCH:
				numberOfCards = 6;
				break;
			case SegmentType.TAG_MATCH:
				numberOfCards = 8;
				break;
			case SegmentType.MIC_SPOT:
				numberOfCards = 7;
				break;
			case SegmentType.SKIT:
				numberOfCards = 7;
				break;
			}
			cards = new CardObject[numberOfCards];
		}

		for (int i = 0;i < cardIDs.Length;i++) {
			if (cards[i] == null && cardIDs[i] > 0) {
				CardObject checkCard = GamePlaySession.instance.CheckCard(cardIDs[i]);

				if (checkCard != null) {
					cards[i] = checkCard;
				} else {
					// If we don't yet have this card, use a dummy version (for tutorial segments)
					cards[i] = items.GetCard(cardIDs[i]).Clone();
				}
			}
		}
	}

    public void GetResults(SegmentObject segment) {
        // If this segment has already had results generated, we don't need to do it again
        
        if (segmentFinished)
            return;
        int thisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
        int thisMonth = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].months[GamePlaySession.instance.currentMonth].monthNum.ToString())+1;

        int thisWeek = 0;

        switch (segmentType){
		case SegmentType.MATCH:
			// Do match calculations!
			if (cards.Length > 0) {
				MatchCardObject matchCard = (MatchCardObject) cards[0];
				FlavorCardObject matchFlavorCard = (FlavorCardObject) cards[1];
				WrestlerCardObject wrestlerCard1 = (WrestlerCardObject) cards[2];
				FlavorCardObject team1FlavorCard = (FlavorCardObject) cards[3];
				WrestlerCardObject wrestlerCard2 = (WrestlerCardObject) cards[4];
				FlavorCardObject team2FlavorCard = (FlavorCardObject) cards[5];

				if (isTitleMatch && titleMatchType == TitleMatchType.CONTINENTAL) {
					segmentTitle = "TV Title Match";
				} else if (isTitleMatch && titleMatchType == TitleMatchType.WORLD) {
					segmentTitle = "World Title Match";
				} else {
					segmentTitle = "Match";
				}

				team1 = new WrestlerCardObject[1] {wrestlerCard1};
				team2 = new WrestlerCardObject[1] {wrestlerCard2};

				segmentWrestlerRatings = new int[team1.Length + team2.Length];
				team1SkillChange = new int[team1.Length];
				team2SkillChange = new int[team2.Length];
				
				// Store the current SKILL stat of each wrestler
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].skillCurrent;
				}
				for (int i = 0;i < team2.Length;i++) {
					team2SkillChange[i] = team2[i].skillCurrent;
				}

				favorites = matchCard.GetMatchFavorite(team1, team1FlavorCard, team2, team2FlavorCard);

				FlavorCardObject fFlavorCard = null;
				FlavorCardObject uFlavorCard = null;
				
				if (favorites == team2) {
					underdogs = team1;
					fFlavorCard = team2FlavorCard;
					uFlavorCard = team1FlavorCard;
				} else {
					underdogs = team2;
					fFlavorCard = team1FlavorCard;
					uFlavorCard = team2FlavorCard;
				}

				matchTarget = matchCard.GetMatchTarget(favorites, team1FlavorCard, underdogs, team2FlavorCard);
                winners = matchCard.GetWinner(favorites, underdogs, matchTarget,segment);
               
				segmentRating = matchCard.RateMatch(favorites, underdogs, matchFlavorCard, fFlavorCard, uFlavorCard, out segmentWrestlerRatings, out segmentBonuses);
				
				// Determine the change in SKILL stat after the above rating calculation
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].skillCurrent - team1SkillChange[i];
				}
				for (int i = 0;i < team2.Length;i++) {
					team2SkillChange[i] = team2[i].skillCurrent - team2SkillChange[i];
				}

				// Handle change over of champions
				if (isTitleMatch && titleMatchType == TitleMatchType.CONTINENTAL)
                    {
                        bool found = false;
                        //TV Title Match
                        if (GamePlaySession.instance.titleChampCardNumber != winners[0].CardNumber)
                        {
                            foreach (CardObject item in GamePlaySession.instance.myCards)
                            {
                                if (item.myCardType == CardType.WRESTLER)
                                {
                                    if (item.CardNumber == GamePlaySession.instance.titleChampion.CardNumber)
                                    {
                                        GamePlaySession.instance.titleChampion.IsChamp=false;
                                    }
                                }
                            }
                            GamePlaySession.instance.titleLastChampion = GamePlaySession.instance.titleChampion;
                        }

                        else if (GamePlaySession.instance.titleChampCardNumber == winners[0].CardNumber)
                        {
                            if (GamePlaySession.instance.AllTitleChampions.Count != 0)
                            {
                                foreach (WrestlerCardObject item in GamePlaySession.instance.AllTitleChampions.ToList())
                                {
                                    if ((item.CardNumber == winners[0].CardNumber))
                                    {
                                        found = true;
                                        item.Defenses++;
                                    }
                                    else
                                    {
                                        found = false;
                                    }
                                }
                            }
                        }
                        if (found == false)
                        {
                            GamePlaySession.instance.AllTitleChampions.Add(winners[0]);
                            winners[0].Defenses = 0;
                            winners[0].CrowningDate = "Month " + thisMonth + ", " + thisYear;
                        }

                        GamePlaySession.instance.titleChampion = winners[0];

				} else if (isTitleMatch && titleMatchType == TitleMatchType.WORLD)
                    {
                        Debug.Log("World Type Match: Winner: " + winners[0].name);
                        bool found = false;
                        //World Title Match
                        if (GamePlaySession.instance.worldChampion.CardNumber != winners[0].CardNumber)
                        {
                            Debug.Log(winners[0].name + " is the new champion");
                            foreach (CardObject item in GamePlaySession.instance.myCards)
                            {
                                if (item.myCardType == CardType.WRESTLER)
                                {
                                    if (item.CardNumber == GamePlaySession.instance.worldChampion.CardNumber)
                                    {
                                        GamePlaySession.instance.worldChampion.IsChamp = false;
                                    }
                                }
                            }
                            GamePlaySession.instance.worldLastChampion = GamePlaySession.instance.worldChampion;
                        }

                        else if (GamePlaySession.instance.worldChampion.CardNumber == winners[0].CardNumber)
                        {
                            Debug.Log(winners[0].name + " is already the champion");
                            if (GamePlaySession.instance.AllWorldChampions.Count != 0)
                            {
                                foreach (WrestlerCardObject item in GamePlaySession.instance.AllWorldChampions.ToList())
                                {
                                    if (item.CardNumber == winners[0].CardNumber)
                                    {
                                        Debug.Log("Found in all world champs, Increasing "+winners[0].name+"'s Defenses..." );
                                        found = true;
                                        item.Defenses++;
                                    }
                                    else
                                    {
                                        found = false;
                                    }
                                }
                            }
                        }

                        if (found == false)
                        {
                            Debug.Log("Not Found in all world champs, Adding");
                            GamePlaySession.instance.AllWorldChampions.Add(winners[0]);
                            winners[0].Defenses = 0;
                            winners[0].CrowningDate = "Month " + thisMonth + ", " + thisYear;
                        }
                        GamePlaySession.instance.worldChampion = winners[0];
                    }
			}
			break;
		case SegmentType.TAG_MATCH:
			if (cards.Length > 0) {
				MatchCardObject matchCard = (MatchCardObject) cards[0];
				FlavorCardObject matchFlavorCard = (FlavorCardObject) cards[1];
				WrestlerCardObject wrestlerCard1 = (WrestlerCardObject) cards[2];
				WrestlerCardObject wrestlerCard2 = (WrestlerCardObject) cards[3];
				FlavorCardObject team1FlavorCard = (FlavorCardObject) cards[4];
				WrestlerCardObject wrestlerCard3 = (WrestlerCardObject) cards[5];
				WrestlerCardObject wrestlerCard4 = (WrestlerCardObject) cards[6];
				FlavorCardObject team2FlavorCard = (FlavorCardObject) cards[7];

				if (isTitleMatch && titleMatchType == TitleMatchType.TAG) {
					segmentTitle = "Tag Title Match";
				} else {
					segmentTitle = "Tag Match";
				}

				team1 = new WrestlerCardObject[2] {wrestlerCard1, wrestlerCard2};
				team2 = new WrestlerCardObject[2] {wrestlerCard3, wrestlerCard4};
				
				segmentWrestlerRatings = new int[team1.Length + team2.Length];
				team1SkillChange = new int[team1.Length];
				team2SkillChange = new int[team2.Length];
				
				// Store the current SKILL stat of each wrestler
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].skillCurrent;
				}
				for (int i = 0;i < team2.Length;i++) {
					team2SkillChange[i] = team2[i].skillCurrent;
				}

				favorites = matchCard.GetMatchFavorite(team1, team1FlavorCard, team2, team2FlavorCard);

				FlavorCardObject fFlavorCard = null;
				FlavorCardObject uFlavorCard = null;

				if (favorites == team2) {
					underdogs = team1;
					fFlavorCard = team2FlavorCard;
					uFlavorCard = team1FlavorCard;
				} else {
					underdogs = team2;
					fFlavorCard = team1FlavorCard;
					uFlavorCard = team2FlavorCard;
				}
				
				matchTarget = matchCard.GetMatchTarget(favorites, team1FlavorCard, underdogs, team2FlavorCard);
				winners = matchCard.GetWinner(favorites, underdogs, matchTarget,segment);
				
				segmentRating = matchCard.RateMatch(favorites, underdogs, matchFlavorCard, fFlavorCard, uFlavorCard, out segmentWrestlerRatings, out segmentBonuses);

				// Determine the change in SKILL stat after the above rating calculation
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].skillCurrent - team1SkillChange[i];
				}
				for (int i = 0;i < team2.Length;i++) {
					team2SkillChange[i] = team2[i].skillCurrent - team2SkillChange[i];
				}

				// Handle change over of champions
				if (isTitleMatch && titleMatchType == TitleMatchType.TAG) {
                        bool found = false;
					// Create previous champion array if it doesn't exist
					if (GamePlaySession.instance.tagLastChampions == null)
						GamePlaySession.instance.tagLastChampions = new WrestlerCardObject[2];

					//Tag Title Match
					if (GamePlaySession.instance.tagChampions[0].CardNumber != winners[0].CardNumber || GamePlaySession.instance.tagChampions[0].CardNumber!=winners[0].CardNumber)
                    {
                        foreach (CardObject item in GamePlaySession.instance.myCards)
                        {
                            if (item.myCardType == CardType.WRESTLER)
                            {
                                if (item.CardNumber == GamePlaySession.instance.tagChampions[0].CardNumber)
                                {
                                    
                                }
                            }
                        }
                        GamePlaySession.instance.tagLastChampions[0] = GamePlaySession.instance.tagChampions[0];
                    }
                    if (GamePlaySession.instance.tagChampions[1].CardNumber != winners[1].CardNumber || GamePlaySession.instance.tagChampions[1].CardNumber != winners[1].CardNumber)
                    {
                        foreach (CardObject item in GamePlaySession.instance.myCards)
                        {
                            if (item.myCardType == CardType.WRESTLER)
                            {
                                if (item.CardNumber == GamePlaySession.instance.tagChampions[1].CardNumber)
                                {
                                    
                                }
                            }
                        }
                        GamePlaySession.instance.tagLastChampions[1] = GamePlaySession.instance.tagChampions[1];
                    }

                    else if (GamePlaySession.instance.tagChampions[0].CardNumber == winners[0].CardNumber || GamePlaySession.instance.tagChampions[1].CardNumber == winners[0].CardNumber)
                    {
                        foreach (WrestlerCardObject item in GamePlaySession.instance.AllTagChampions.ToList())
                        {
                            if (item.CardNumber == winners[0].CardNumber && winners[0].IsChamp==true)
                            {
                                found = true;
                                item.Defenses++;
                            }
                            else
                            {
                                found = false;
                            }
                        }
                    }
                    if (found == false)
                    {
                        GamePlaySession.instance.AllTagChampions.Add(winners[0]);
                        winners[0].Defenses = 0;
                        //winners[0].CrowningDate = "Week " + thisWeek + "Month " + thisMonth + ", " + thisYear;
                        winners[0].CrowningDate = "Month " + thisMonth + ", " + thisYear;
                    }
                    if (GamePlaySession.instance.tagChampions[0].CardNumber == winners[1].CardNumber || GamePlaySession.instance.tagChampions[1].CardNumber == winners[1].CardNumber)
                    {
                        foreach (WrestlerCardObject item in GamePlaySession.instance.AllTagChampions.ToList())
                        {
                            if (item.CardNumber == winners[1].CardNumber && winners[1].IsChamp == true)
                            {
                                found = true;
                                item.Defenses++;
                            }
                            else
                            {
                                found = false;
                            }
                        }
                    }
                    if (found == false)
                    {
                        GamePlaySession.instance.AllTagChampions.Add(winners[1]);
                        winners[1].Defenses = 0;
                        winners[1].CrowningDate = "Month " + thisMonth + ", " + thisYear;
                    }
                    GamePlaySession.instance.tagChampions[0] = winners[0];
					GamePlaySession.instance.tagChampions[1] = winners[1];
				}
			}
			break;
		case SegmentType.SKIT:
			if (cards.Length > 0) {
				SkitCardObject skitCard = (SkitCardObject) cards[0];
				FlavorCardObject skitFlavorCard = (FlavorCardObject) cards[6];

				segmentTitle = skitCard.name;
				
				List<WrestlerCardObject> teamList = new List<WrestlerCardObject>();
				
				for (int i = 1;i <= 5;i++) {
					if (cards[i] != null)
						teamList.Add((WrestlerCardObject) cards[i]);
				}
				
				team1 = teamList.ToArray();
				
				segmentWrestlerRatings = new int[team1.Length];
				team1SkillChange = new int[team1.Length];

				// Store the current POP stat of each wrestler
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].popCurrent;
				}

				segmentRating = skitCard.RateSkit(team1, skitFlavorCard, out segmentWrestlerRatings, out segmentBonuses);

				segmentPassed = skitCard.lastCheckPassed;
				
				// Check if this was a dud, and if so fail it automatically
				if (GetStarRating() == 0f)
					segmentPassed = false;

				// Determine the change in POP stat after the above rating calculation
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].popCurrent - team1SkillChange[i];
				}
			}
			break;
		case SegmentType.MIC_SPOT:
			if (cards.Length > 0) {
				MicSpotCardObject micSpotCard = (MicSpotCardObject) cards[0];
				FlavorCardObject micSpotFlavorCard = (FlavorCardObject) cards[6];

				segmentTitle = micSpotCard.name;

				List<WrestlerCardObject> teamList = new List<WrestlerCardObject>();

				for (int i = 1;i <= 5;i++) {
					if (cards[i] != null)
						teamList.Add((WrestlerCardObject) cards[i]);
				}

				team1 = teamList.ToArray();
				
				segmentWrestlerRatings = new int[team1.Length];
				team1SkillChange = new int[team1.Length];
				
				// Store the current MIC stat of each wrestler
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].micCurrent;
				}

				segmentRating = micSpotCard.RateMicSpot(team1, micSpotFlavorCard, out segmentWrestlerRatings, out segmentBonuses);
				
				segmentPassed = micSpotCard.lastCheckPassed;

				// Check if this was a dud, and if so fail it automatically
				if (GetStarRating() == 0f)
					segmentPassed = false;
				
				// Determine the change in MIC stat after the above rating calculation
				for (int i = 0;i < team1.Length;i++) {
					team1SkillChange[i] = team1[i].micCurrent - team1SkillChange[i];
				}
			}
			break;
		}

		SaveSerializeReferences();
		segmentFinished = true;
	}

	public float GetStarRating() {
		if (segmentRating >= 19f) {
			// 5 stars
			return 1f;
		} else if (segmentRating >= 15f) {
			// 4 stars
			return 0.8f;
		} else if (segmentRating >= 11f) {
			// 3 stars
			return 0.6f;
		} else if (segmentRating >= 7f) {
			// 2 stars
			return 0.4f;
		} else if (segmentRating >= 3f) {
			// 1 star
			return 0.2f;
		} else {
			// 0 stars
			return 0f;
		}

	}
	public void SaveSerializeReferences(){
		if(cards != null){
			cardIDs = new int[cards.Length];
			for(int i = 0; i < cards.Length; i++){
				if(cards[i] != null){
					cardIDs[i] = cards[i].CardNumber;
				}
			}
		}
		if(team1 != null){
			team1IDs = new int[team1.Length];
			for(int i = 0; i < team1.Length; i++){
				team1IDs[i] = team1[i].CardNumber;
			}
		}
		if(team2 != null){
			team2IDs = new int[team2.Length];
			for(int i = 0; i < team2.Length; i++){
				team2IDs[i] = team2[i].CardNumber;
			}
		}
		if(favorites != null){
			favoriteIDs = new int[favorites.Length];
			for(int i = 0; i < favorites.Length; i++){
				favoriteIDs[i] = favorites[i].CardNumber;
			}
		}
		if(underdogs != null){
			underdogIDs = new int[underdogs.Length];
			for(int i = 0; i < underdogs.Length; i++){
				underdogIDs[i] = underdogs[i].CardNumber;
			}
		}
		if(winners != null){
			winnerIDs = new int[winners.Length];
			for(int i = 0; i < winners.Length; i++){
				winnerIDs[i] = winners[i].CardNumber;
			}
		}
	}
	public void OnBeforeSerialize(){

	}
	
	public void OnAfterDeserialize()
	{
	}
	public void PostDeserialize()
	{
		ProcessCardIDs();

		if(team1IDs != null){
			team1 = new WrestlerCardObject[team1IDs.Length];
			for(int i = 0; i < team1IDs.Length; i++){
				team1[i] = LookupCard(team1IDs[i]);
			}
		}
		if(team2IDs != null){
			team2 = new WrestlerCardObject[team2IDs.Length];
			
			for(int i = 0; i < team2IDs.Length; i++){
				team2[i] = LookupCard(team2IDs[i]);
			}
		}

		if(favoriteIDs != null){
			favorites = new WrestlerCardObject[favoriteIDs.Length];
			for(int i = 0; i < favoriteIDs.Length; i++){
				favorites[i] = LookupCard(favoriteIDs[i]);
			}
		}
		if(underdogIDs != null){
			underdogs = new WrestlerCardObject[underdogIDs.Length];
			for(int i = 0; i < underdogIDs.Length; i++){
				underdogs[i] = LookupCard(underdogIDs[i]);
			}
		}
		if(winnerIDs != null){
			winners = new WrestlerCardObject[winnerIDs.Length];
			for(int i = 0; i < winnerIDs.Length; i++){
				winners[i] = LookupCard(winnerIDs[i]);
			}
		}

		//Debug.Log("Segment recreated!");

	}

	public WrestlerCardObject LookupCard(int id){
		for(int i = 0 ; i < cards.Length; i++){
			if(cards[i] != null){
				if(cards[i].CardNumber == id){
					return (WrestlerCardObject) cards[i];
				}
			}

		}
		Debug.Log("blargh not found");
		return null;
	}

}