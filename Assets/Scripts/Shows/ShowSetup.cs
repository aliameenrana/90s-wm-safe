using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SegmentTypeDisplay {
	public SegmentObject.SegmentType segmentType;
	public SegmentDisplay segmentDisplay;
}

public class ShowSetup : MonoBehaviour {
	public SegmentTypeDisplay[] segmentTypeDisplays;
	
	public WeekItem currentWeek;

	public ShowTypePanel showTypePanel;

	public GameObject showVenueButton;
	public Image showVenueImage;
	public CardSelector showVenueSelector;

	public GameObject segmentAnchor;
	public ScrollRect segmentScroll;
	
	public Dictionary<int, SegmentDisplay> segments = new Dictionary<int, SegmentDisplay>();

	public Button startShowButton;

	public CardSelectScreenScript cardViewer;

	public int teamFeudTutorialWeek;
	public PopUpMessage tagTeamTutorialMessage;
	public PopUpMessage feudTutorialMessage;

	public PopUpMessage teamFeudUnlockMessage;
	public PopUpMessage teamUnlockMessage;
	public PopUpMessage feudUnlockMessage;

	private Hashtable segmentTypeHash;

	private UIManagerScript managerScript;
	private GameObject playerHUD;

	void OnEnable(){
		if(managerScript == null){
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
	}
	void Update() {
		// Check if the start show button should be on or off
		if (startShowButton != null) {
			if (CheckShowReady()) {
				startShowButton.interactable = true;
			} else {
				startShowButton.interactable = false;
			}
		}
	}

	void GenerateHashtable() {
		//Debug.Log("GenerateHashtable()");
		// Generate a hashtable of card back prefabs if one isn't present
		if (segmentTypeHash != null)
			return;
		
		segmentTypeHash = new Hashtable();
		foreach (SegmentTypeDisplay display in segmentTypeDisplays) {
			segmentTypeHash.Add(display.segmentType, display.segmentDisplay);
		}
	}

	public void ReturnToCalendar() {
		//Debug.Log("ReturnToCalendar");
		if(managerScript == null){
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
		if (GamePlaySession.instance.myShows[currentWeek.showID] != null) {
			RefundCards();
			GamePlaySession.instance.myShows[currentWeek.showID] = currentWeek.GenerateShow();
		}

		segmentScroll.verticalNormalizedPosition = 1f;
		managerScript.GoToCalendar(false);
	}

	public void ReturnToHome() {
		//Debug.Log("ReturnToHome()");
		if(managerScript == null){
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
		if (GamePlaySession.instance.myShows[currentWeek.showID] != null) {
			RefundCards();
			GamePlaySession.instance.myShows[currentWeek.showID] = currentWeek.GenerateShow();
		}

		segmentScroll.verticalNormalizedPosition = 1f;
		managerScript.HomeButton();
	}

	public void RefundCards() {
		//Debug.Log("RefundCards");
		// Get current show
		ShowObject show = GamePlaySession.instance.myShows[currentWeek.showID];

		// Return venue cost
		if (show.venueCard != 0)
			GamePlaySession.instance.myCash += GamePlaySession.instance.CheckCard(show.venueCard).GetPlayCost();

		// Return any segment card costs
		foreach (SegmentObject segment in show.segments) {
			if (segment.cards != null) {
				foreach (CardObject card in segment.cards) {
					if (card != null) {
						GamePlaySession.instance.myCash += card.GetPlayCost();
					}
				}
			}
		}
	}

	public void OpenShowSetupPanel(WeekItem week) {
		//Debug.Log("OpenShowSetupPanel()");
		if(managerScript == null)
        {
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
		playerHUD = managerScript.PlayerHUD;
		
		if (playerHUD != null)
			playerHUD.SetActive(false);
		
		SetCurrentShow(week);

		if (segmentScroll != null) {
			//Debug.Log("reset scroll!");
			//segmentScroll.verticalNormalizedPosition = 0f;
			segmentScroll.verticalScrollbar.value = 0f;
		}
		
		if (ScreenHandler.current != null)
			ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());
	}

	public void ReturnToPanel() {
		Debug.Log("ReturnToPanel");
		if(managerScript == null){
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
		playerHUD = managerScript.PlayerHUD;
		
		if (playerHUD != null)
			playerHUD.SetActive(false);

		UpdateShowOutput();
	}

	public void SetCurrentShow(WeekItem week) {
		//Debug.Log("SetCurrentShow");
		// Set new show data and wipe existing segments
		currentWeek = week;

		if (GamePlaySession.instance.myShows.Count <= currentWeek.showID) {
			GamePlaySession.instance.myShows.Insert(currentWeek.showID, currentWeek.GenerateShow());
		} else if (GamePlaySession.instance.myShows[currentWeek.showID] == null) {
			GamePlaySession.instance.myShows[currentWeek.showID] = currentWeek.GenerateShow();
		} else if (GamePlaySession.instance.myShows[currentWeek.showID] != null) {
			RefundCards();
			GamePlaySession.instance.myShows[currentWeek.showID] = currentWeek.GenerateShow();
		}

		foreach (KeyValuePair<int, SegmentDisplay> segment in segments) {
			Object.Destroy(segment.Value.gameObject);
		}
		segments.Clear();

		UpdateShowOutput();
	}

	public bool CheckShowReady() {
		bool isReady = true;

		if (GamePlaySession.instance == null || currentWeek == null) {
			isReady = false;
		} else {
			if (currentWeek.showID < GamePlaySession.instance.myShows.Count) {
				ShowObject show = GamePlaySession.instance.myShows[currentWeek.showID];
				
				if (show.venueCard == 0)
					isReady = false;
				
				for (int i = 0;i < show.segments.Length;i++) {
					SegmentObject segment = show.segments[i];
					if (segment.isPendingSetup == true)
						isReady = false;
				}
			}
		}

		return isReady;
	}

	public void StartShow() {
		Debug.Log("StartShow()");
		if(managerScript == null){
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
		if (GamePlaySession.instance.myShows[currentWeek.showID] == null)
			return;

		ShowObject show = GamePlaySession.instance.myShows[currentWeek.showID];
		
		// Process the show results and set up the result screen
		show.GenerateShowResults();
		GamePlaySession.instance.myCash += show.showEarnings;

		Animator resultsScreen = managerScript.ShowResultsScreen;
		Animator transitionScreen = managerScript.ShowTransitionScreen;
		ShowResults showResults = resultsScreen.gameObject.GetComponent<ShowResults>();
		TransitionScreen transition = transitionScreen.gameObject.GetComponent<TransitionScreen>();

		showResults.SetResultsData(currentWeek);
		showResults.oldResult = false;
		transition.needSave = true;
		transition.show = show;

		segmentScroll.verticalNormalizedPosition = 1f;

		transition.OpenScreen();
	}

	public void UpdateVenueOutput() {
		//Debug.Log("UpdateVenueOutput");
		if (GamePlaySession.instance.myShows[currentWeek.showID] == null)
			return;

		ShowObject show = GamePlaySession.instance.myShows[currentWeek.showID];

		if (showVenueSelector != null) {
			// Set up the selector
			showVenueSelector.cards.Clear();
			foreach (CardObject card in GamePlaySession.instance.myCards) {
				if (CheckCardPlayable(card)) {
					showVenueSelector.cards.Add(card);
				}
			}
		}

		if (show.venueCard != 0) {
			if (showVenueImage != null && showVenueButton != null) {
				CardObject venue = GamePlaySession.instance.CheckCard(show.venueCard);

				if (venue != null) {
					showVenueImage.sprite = venue.headshotImage;
					showVenueImage.transform.parent.gameObject.SetActive(true);
					showVenueButton.SetActive(false);
				}
			}
		} else {
			if (showVenueImage != null && showVenueButton != null) {
				showVenueImage.transform.parent.gameObject.SetActive(false);
				showVenueButton.SetActive(true);
			}
		}
	}

	public bool CheckCardPlayable(CardObject card) {
		ShowObject show = GamePlaySession.instance.myShows[currentWeek.showID];

		bool isPlayable = true;

		// Check if venue card is playable in current show
		if (card.myCardType == CardType.VENUE) {
			VenueCardObject venue = (VenueCardObject) card;
			if (show.isP4V) {
				if (venue.venueLimit == "REG") {
					isPlayable = false;
				}
			} else {
				if (venue.venueLimit == "P4V") {
					isPlayable = false;
				}
			}
		}

		return isPlayable;
	}

	public void UpdateShowOutput() {
		//Debug.Log("UpdateShowOutput");
		if(managerScript == null){
			managerScript = GamePlaySession.instance.GSI.GetComponent<UIManagerScript>();
		}
		if (GamePlaySession.instance.myShows[currentWeek.showID] == null)
			return;

		ShowObject show = GamePlaySession.instance.myShows[currentWeek.showID];
		
		// Generate our hashtable of segment displays
		GenerateHashtable();

		// Set the current show name
		if (showTypePanel != null) {
			// Initially set the title to be active and no header display
			showTypePanel.showTypeText.text = show.displayTitle;
			showTypePanel.showTypeImage.transform.parent.gameObject.SetActive(false);
			showTypePanel.showTypeText.gameObject.SetActive(true);

			// Check if we should have a header display
			ShowIcons showIcons = managerScript.GetComponent<ShowIcons>();
			if (showIcons != null) {
				if (showIcons.showIconsets.Length >= currentWeek.showTypeID) {
					ShowIconset iconset = showIcons.showIconsets[currentWeek.showTypeID];
                    int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                    if (ThisYear < 1990)
                    {
                        if (iconset.showHeader != null)
                        {
                            // We found a header image so let's switch to that
                            showTypePanel.showTypeImage.sprite = iconset.showHeader;
                            showTypePanel.showTypeImage.transform.parent.gameObject.SetActive(true);
                            showTypePanel.showTypeText.gameObject.SetActive(false);
                        }
                    }
                    else if (ThisYear >= 1990)
                    {
                        if (iconset.showHeader != null)
                        {
                            // We found a header image so let's switch to that
                            showTypePanel.showTypeImage.sprite = iconset.showHeader90s;
                            showTypePanel.showTypeImage.transform.parent.gameObject.SetActive(true);
                            showTypePanel.showTypeText.gameObject.SetActive(false);
                        }
                    }
				}
			}
		}

		UpdateVenueOutput();

		// Process segment display objects
		for (int i = 0;i < show.segments.Length;i++) {
			SegmentObject segment = show.segments[i];

			if (segmentTypeHash.ContainsKey(segment.segmentType)) {
				if (segments.ContainsKey(i)) {
					Object.Destroy(segments[i].gameObject);
					segments.Remove(i);
				}

				SegmentDisplay display = SegmentDisplay.Instantiate(segmentTypeHash[segment.segmentType] as SegmentDisplay, Vector3.zero, new Quaternion()) as SegmentDisplay;
				
				// Set the parent to the segment anchor and show display
				display.transform.SetParent(segmentAnchor.transform, false);
				display.showSetup = this;
				display.segmentID = i;
				display.ShowSegment();
				
				segments.Add(i, display);
			}
		}
	}
}