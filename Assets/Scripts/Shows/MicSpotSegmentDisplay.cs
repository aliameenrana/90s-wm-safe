﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MicSpotSegmentDisplay : SegmentDisplay {

	public Text micSpotName;
	public Text[] wrestlerNames;
	public Image[] wrestlerHeadshots;
	
	public override void OpenSegmentSetup() {
		base.OpenSegmentSetup();

		UIManager = GameObject.Find("UIManager");
		Animator micSpotScreen = UIManager.GetComponent<UIManagerScript>().MicSpotSetupScreen;
		MicSpotSegmentSetup micSpotSetup = micSpotScreen.gameObject.GetComponent<MicSpotSegmentSetup>();
		
		if (ScreenHandler.current != null && micSpotScreen != null)
			ScreenHandler.current.OpenPanel(micSpotScreen);
		
		micSpotSetup.SetData(this);
	}
	
	public override void UpdateSegmentData() {
		SegmentObject segment = GamePlaySession.instance.myShows[showSetup.currentWeek.showID].segments[segmentID];
		
		if (segment != null) {
			CardObject micSpotCard = segment.cards[0];

			if (micSpotCard != null) {
				micSpotName.text = micSpotCard.name;
			}
			
			for (int i = 1;i <= 5;i++) {
				if (segment.cards[i] != null) {
					wrestlerNames[i-1].gameObject.SetActive(true);
					wrestlerHeadshots[i-1].transform.parent.gameObject.SetActive(true);
					
					WrestlerCardObject wrestler = (WrestlerCardObject) segment.cards[i];
					wrestlerNames[i-1].text = wrestler.name;
					wrestlerHeadshots[i-1].sprite = wrestler.headshotImage;
				} else {
					wrestlerNames[i-1].gameObject.SetActive(false);
					wrestlerHeadshots[i-1].transform.parent.gameObject.SetActive(false);
				}
			}
		}
	}
}
