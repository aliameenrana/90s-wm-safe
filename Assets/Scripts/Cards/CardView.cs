﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

[System.Serializable]
public class CardBackType {
	public CardType cardType;
	public CardViewBack backPrefab;
}

public class CardView : MonoBehaviour {

    public CardObject myCard;

    public Image cardFront;
    public Image champBelt;
    public Sprite[] Belts;
    public GameObject cardBack;

    public CardBackType[] backTypes;

    private Animator anim;
    private Hashtable backTypeHash;
    private CardViewBack cardBackObject;

    public AudioClip tagMatchClip;
    public AudioClip feudClip;
    public AudioClip matchClip;
    public AudioClip micSpotClip;
    public AudioClip skitClip;
    public AudioClip sponsorClip;
    public AudioClip tagTeamClip;
    public AudioClip venueClip;

    private AudioSource aSource;

    private AudioSource UIManagerAudio;
    private UIManagerScript UIMS;
    private Graphic[] graphics;

    private bool animFlip;
    public bool first;

    private bool firstClick;

    public CardViewBack tempcbo;

    void Awake() {
        anim = this.gameObject.GetComponent<Animator>() as Animator;
        aSource = this.GetComponent<AudioSource>();
        UIManagerAudio = GameObject.Find("UIManager").GetComponent<AudioSource>();
        UIMS = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
        animFlip = true;
    }

    void Start() {
        if (first) {
            graphics = tempcbo.GetComponentsInChildren<Graphic>();
            FlipCard();
        }
        //anim.Play("Front");
        //anim.playbackTime = 0f;
    }

    void MakeCardBackInvisible()
    {
        if (graphics == null)
            return;

        for (int i = 0; i < graphics.Length; i++)
        {
            if(graphics[i] != null)
                graphics[i].enabled = false;
        }
    }

    void MakeCardBackVisible() {
        for (int i = 0; i < graphics.Length; i++) {
            try {
                graphics[i].enabled = true;
            }
            catch (System.Exception e) {
                Debug.Log("I am Catbug");
            }
        }
    }

    void Update() {
        if (aSource.isPlaying) {
            UIManagerAudio.volume = .25f;
        }
        else {
            UIManagerAudio.volume = .75f;
        }
        //if (Input.GetKey(KeyCode.O))
        //{
        //    foreach (var item in graphics)
        //    {
        //        Debug.Log(item.name);
        //    }
        //}
    }

    void GenerateHashtable() {
        // Generate a hashtable of card back prefabs if one isn't present
        if (backTypeHash != null)
            return;

        backTypeHash = new Hashtable();
        foreach (CardBackType back in backTypes) {
            backTypeHash.Add(back.cardType, back.backPrefab);
        }
    }

    public void FlipCard()
    {


        //if (cardBackObject != null)
        //cardBackObject.UpdateBackInfo(myCard);

        animFlip = !animFlip;
        anim.SetBool("Flipped", !anim.GetBool("Flipped"));
        if (first)
        {
            return;
        }
        int cardNum = (myCard.CardNumber % 2 == 1) ? myCard.CardNumber : (myCard.CardNumber - 1);
        if (anim.GetBool("Flipped"))
        {
            Debug.Log(myCard.myCardType);
            switch (myCard.myCardType)
            {
                case CardType.FEUD:
                    aSource.clip = feudClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.MATCH:
                    aSource.clip = matchClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.TAG_TEAM:
                    aSource.clip = tagTeamClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.MIC_SPOT:
                    aSource.clip = micSpotClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.SKIT:
                    aSource.clip = skitClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.SPONSOR:
                    aSource.clip = sponsorClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.VENUE:
                    aSource.clip = venueClip;
                    if (UIMS.sfxEnabled)
                    {
                        aSource.Play();
                    }
                    break;
                case CardType.WRESTLER:
                    StartCoroutine(PlaySoundFX(cardNum));
                    break;
                case CardType.MANAGER:
                    StartCoroutine(PlaySoundFX(myCard.CardNumber));
                    break;
                default:
                    return;
            }

        }
    }

    public IEnumerator PlaySoundFX(int cn) {
        if (cn != 406)
        {
            aSource.clip = GamePlaySession.instance.GSI.cardAudioDict[cn];
        }
        Debug.Log(aSource.clip);
        if (UIMS.sfxEnabled) {
            try
            {
                aSource.Play();
            }
            catch
            {

            }
        }
        yield return null;
    }
    public void ShowCard(CardObject card) {
        // Display the card front
        myCard = card;
        CardType cardType = myCard.myCardType;
        
        cardFront.sprite = myCard.frontImage;
        GamePlaySession G = GamePlaySession.instance;

        if (myCard.CardNumber == G.tagChampCardNumbers[0] || myCard.CardNumber == G.tagChampCardNumbers[1])
        {
            champBelt.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            champBelt.GetComponent<Image>().sprite = Belts[0];
            champBelt.GetComponent<Image>().SetNativeSize();
        }
        else if (myCard.CardNumber == G.titleChampCardNumber)
        {
            champBelt.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            champBelt.GetComponent<Image>().sprite = Belts[1];
            champBelt.GetComponent<Image>().SetNativeSize();
        }
        else if (myCard.CardNumber == G.worldChampCardNumber)
        {
            champBelt.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            champBelt.GetComponent<Image>().sprite = Belts[2];
            champBelt.GetComponent<Image>().SetNativeSize();
        }
        else
        {
            champBelt.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }


        // Finally generate our card back if a prefab is present in the hashtable
        GenerateHashtable();
		if (backTypeHash.ContainsKey(cardType)) {
			// Delete the existing card back prefab if present
			if (cardBackObject != null)
				Object.Destroy(cardBackObject.gameObject);

			cardBackObject = CardViewBack.Instantiate(backTypeHash[cardType] as CardViewBack, Vector3.zero, new Quaternion()) as CardViewBack;

			// Set the parent to the card back and update info
			cardBackObject.transform.SetParent(cardBack.transform, false);
			cardBackObject.UpdateBackInfo(myCard);
		}

        graphics = cardBackObject.GetComponentsInChildren<Graphic>();

		if(anim != null && !anim.GetCurrentAnimatorStateInfo(0).IsName("Back")){
			MakeCardBackInvisible();
		}

		//Debug.Log("Card: " + card.name);
		//Debug.Log("Front Image = " + "Cards_Front/"+card.CardNumber.ToString("D3")+"_wrestler("+Regex.Replace(card.name,  @"[^\w\@-]", string.Empty)+")");
		//Debug.Log("Logo Image = " + "Cards_Back/"+card.CardNumber.ToString("D3")+"_logo("+Regex.Replace(card.name, @"[^\w\@-]", string.Empty)+")");
		//Debug.Log("Headshot Image = " + "Cards_Back/"+card.CardNumber.ToString("D3")+"_headshot("+Regex.Replace(card.name, @"[^\w\@-]", string.Empty)+")");

	}

	void OnEnable() {
		if(cardBackObject == null){
			return;
		}
		graphics = cardBackObject.GetComponentsInChildren<Graphic>();
		//MakeCardBackVisible();
		
		if(anim != null && !anim.GetBool("Flipped")){
			MakeCardBackInvisible();
		}
	}


}
