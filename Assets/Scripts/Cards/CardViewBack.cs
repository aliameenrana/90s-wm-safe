﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class CardViewBack : MonoBehaviour {
	public Color goodColor;
	public Color badColor;
	
	public Sprite regCardBackSprite;
	public Sprite foilCardBackSprite;

	public abstract void UpdateBackInfo(CardObject card);
}
