﻿using UnityEngine;
using System.Collections;

public class TagMatchSetupCardSelector : CardSelector {
	public TagMatchSegmentSetup tagMatchSetup;
	
	public override void SelectCard(CardObject card) {
		tagMatchSetup.UpdateOption(optionIndex, card);
	}
	
	public override CardObject GetCard () {
		return tagMatchSetup.cards[optionIndex];
	}
	
	public override void ResetCard() {
		tagMatchSetup.UpdateOption(optionIndex, null);
	}
}
