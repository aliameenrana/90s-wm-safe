﻿using UnityEngine;
using System.Collections;

public class ShowVenueCardSelector : CardSelector {
	public ShowSetup showSetup;

	public override void SelectCard(CardObject card) {
		GamePlaySession.instance.myShows[showSetup.currentWeek.showID].venueCard = card.CardNumber;
		showSetup.UpdateVenueOutput();
	}

	public override CardObject GetCard() {
		return GamePlaySession.instance.CheckCard(GamePlaySession.instance.myShows[showSetup.currentWeek.showID].venueCard);
	}
	
	public override void ResetCard() {
		GamePlaySession.instance.myShows[showSetup.currentWeek.showID].venueCard = 0;
		showSetup.UpdateVenueOutput();
	}
}
