﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackSkit : CardViewBack {
	
	public Text cardTitle;
	public Image logoSprite;
	public Image headshotSprite;
	
	public Text wrestlerTypeText;
	public Text minWrestlerText;
	public Text maxWrestlerText;
	
	public Text flavorText;
	
	public override void UpdateBackInfo(CardObject card) {
		// Set card back
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		cardBackImage.sprite = regCardBackSprite;

		cardTitle.text = card.name;

		logoSprite.sprite = card.logoImage;
		headshotSprite.sprite = card.headshotImage;
		
		SkitCardObject sco = (SkitCardObject) card;
		
		wrestlerTypeText.text = sco.wrestlerType;
		minWrestlerText.text = sco.wrestlerMin.ToString();
		maxWrestlerText.text = sco.wrestlerMax.ToString();
		
		flavorText.text = sco.flavorText;
	}
}
