﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackWrestler : CardViewBack {

	public Image logoSprite;
	public Image headshotSprite;

	public Text statSkill;
	public Text statPush;
	public Text statMic;
	public Text statPop;
	
	public Text statusData;
	public Text styleData;

	public Text fanData;
	public Text otherStatsData;
	public Text cardDescription;
    public Text CareerYears;

	public LayoutElement beltImages;
	public Image worldBeltImage;
	public Image titleBeltImage;
	public Image tagBeltImage;

	public Sprite worldChampionSprite;
	public Sprite titleChampionSprite;
	public Sprite tagChampionSprite;

    public GameObject PromoteStatusButton;
    public GameObject DemoteStatusButton;
    public GameObject FoilButton;
    public static CardViewBackWrestler instance;

	GameSetupItems items;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        PromoteStatusButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.PromoteWrestlerStatus());
        DemoteStatusButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.DemoteWrestlerStatus());
        FoilButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.ShowFoilCard());
    }


    public override void UpdateBackInfo(CardObject card) {

		WrestlerCardObject wrestler = (WrestlerCardObject) card;

		// Check if card is foil
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		if (wrestler.cardVersion == "FOIL") {
			cardBackImage.sprite = foilCardBackSprite;
		} else {
			cardBackImage.sprite = regCardBackSprite;
		}

        if (wrestler.cardVersion.ToUpper() != "FOIL")
        {
            foreach (CardObject C in GamePlaySession.instance.myCards)
            {
                if (C.myCardType == CardType.WRESTLER)
                {
                    WrestlerCardObject W = (WrestlerCardObject)C;
                    if (W.characterNumber == wrestler.characterNumber && W.CardNumber == wrestler.CardNumber + 1)
                    {
                        if (W.cardVersion.ToUpper() == "FOIL")
                        {
                            FoilButton.GetComponent<Button>().interactable = true;
                        }
                        else
                        {
                            FoilButton.GetComponent<Button>().interactable = false;
                        }
                    }
                }
            }
        }
        else if (wrestler.cardVersion.ToUpper() == "FOIL")
        {
            foreach (CardObject C in GamePlaySession.instance.myCards)
            {
                if (C.myCardType == CardType.WRESTLER)
                {
                    WrestlerCardObject W = (WrestlerCardObject)C;
                    if (W.characterNumber == wrestler.characterNumber && W.CardNumber == wrestler.CardNumber - 1)
                    {
                        if (W.cardVersion.ToUpper() != "FOIL")
                        {
                            FoilButton.GetComponent<Button>().interactable = true;
                        }
                        else
                        {
                            FoilButton.GetComponent<Button>().interactable = false;
                        }
                    }
                }
            }
        }

        logoSprite.sprite = wrestler.logoImage;
		headshotSprite.sprite = wrestler.headshotImage;

		statusData.text = wrestler.characterStatus;

		styleData.text = wrestler.wrestlerType;

		fanData.text = wrestler.cardAlignment;
        CareerYears.text = "Career Years: "+"("+wrestler.FirstActiveYear + " - " + wrestler.LastActiveYear+")"; 
		if (wrestler.cardAlignment == "Good") {
			fanData.color = goodColor;
		} else {
			fanData.color = badColor;
		}

        if (wrestler.skillCurrent >= wrestler.stats.SkillMax)
        {
            statSkill.text = (wrestler.skillCurrent.ToString()) + ("<color=#ff0000>(MAX)</color>");
            statSkill.GetComponent<Text>().resizeTextForBestFit = true;
        }
        else if (wrestler.skillCurrent < wrestler.stats.SkillMax)
        {
            statSkill.text = (wrestler.skillCurrent.ToString());
        }

        if (wrestler.micCurrent >= wrestler.stats.MicMax)
        {
            statMic.text = (wrestler.skillCurrent.ToString()) + ("<color=#ff0000>(MAX)</color>");
            statMic.GetComponent<Text>().resizeTextForBestFit = true;

        }

        else if (wrestler.micCurrent < wrestler.stats.MicMax)
        {
            statMic.text = (wrestler.micCurrent.ToString());
        }

        if (wrestler.popCurrent >= wrestler.stats.PopMax)
        {
            statPop.text = (wrestler.popCurrent.ToString()) + ("<color=#ff0000>(MAX)</color>");
            statPop.GetComponent<Text>().resizeTextForBestFit = true;
        }

        else if (wrestler.popCurrent < wrestler.stats.PopMax)
        {
            statPop.text = (wrestler.popCurrent.ToString());
        }

		statPush.text = wrestler.pushCurrent.ToString();

		otherStatsData.text = wrestler.characterHeight + " " + wrestler.characterWeight + " " + wrestler.characterHometown;
		cardDescription.text = wrestler.flavorText;

		if (beltImages != null) {
			bool worldChamp = false;
			bool titleChamp = false;
			bool tagChamp = false;

			if (GamePlaySession.instance != null) {
                if (GamePlaySession.instance.worldChampion != null && GamePlaySession.instance.worldChampCardNumber== wrestler.CardNumber) {
					worldChamp = true;
				}
				if (GamePlaySession.instance.titleChampion != null && GamePlaySession.instance.titleChampCardNumber == wrestler.CardNumber)
                {
                    titleChamp = true;
				}
				if (GamePlaySession.instance.tagChampions != null)
                {
                    if (wrestler.CardNumber == GamePlaySession.instance.tagChampCardNumbers[0] || wrestler.CardNumber == GamePlaySession.instance.tagChampCardNumbers[1])
                    {
                        tagChamp = true;
                    }	
				}

				if (worldChamp || titleChamp || tagChamp)
                {
					beltImages.preferredHeight = 40;
					statusData.text = "CHAMP";
					if (worldChamp) {
						beltImages.preferredHeight += 20;
						worldBeltImage.gameObject.SetActive(true);
					} else {
						worldBeltImage.gameObject.SetActive(false);
					}
					if (titleChamp) {
                        beltImages.preferredHeight += 20;
						titleBeltImage.gameObject.SetActive(true);
					} else {
						titleBeltImage.gameObject.SetActive(false);
					}
					if (tagChamp) {
						beltImages.preferredHeight += 20;
						tagBeltImage.gameObject.SetActive(true);
					} else {
						tagBeltImage.gameObject.SetActive(false);
					}
				} else {
					beltImages.preferredHeight = 0;
					worldBeltImage.gameObject.SetActive(false);
					titleBeltImage.gameObject.SetActive(false);
					tagBeltImage.gameObject.SetActive(false);
				}
			}
		}
	}

	public void SetAverageImage(Image img, float avg){
		img.fillAmount = avg;
	}
}
