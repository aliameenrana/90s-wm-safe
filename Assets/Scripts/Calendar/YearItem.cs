﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class YearItem {
	public int yearNum;
	public string yearName;

	public MonthItem[] months;

	public YearItem(){
		months = new MonthItem[12];
	}
}
