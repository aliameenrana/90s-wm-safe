﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MonthItem {
	public int monthNum;
	public string monthName;
    public static MonthItem instance;

    void Awake()
    {
        instance = this;
    }

	public WeekItem[] weeks;

	public MonthItem(){
	}
}
