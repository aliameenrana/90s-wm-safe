﻿using UnityEngine;
using System.Collections;

public class CalendarGenerator : MonoBehaviour {
	public int numberOfYears; /* The number of years to generate */
	public int nowYouKnowShowType;  /* This is the ID of the Now You Know segments */
	public int tutorialWeek1ShowType; /* This is the ID of the show for tutorial week 1 */
	public int tutorialWeek2ShowType; /* This is the ID of the show for tutorial week 2 */
	public int tutorialWeek3ShowType; /* This is the ID of the show for tutorial week 3 */
	public int tutorialWeek4ShowType; /* This is the ID of the show for tutorial week 4 */
	public int[] week1ShowTypes; /* These are general show type IDs and will be selected from at random */
	public int[] week2ShowTypes; /* These are general show type IDs and will be selected from at random */
	public int[] week3ShowTypes; /* These are general show type IDs and will be selected from at random */
	public int[] week4ShowTypes; /* These are P4V show type IDs and will be selected based on month number */
}
