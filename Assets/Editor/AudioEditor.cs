﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class AudioEditor : Editor {

	[MenuItem ("Custom/Sound/Toggle 3D sound/Disable")]
	static void Toggle3DSound_Disable() {
		SelectedToggle3DSoundSettings(false);
	}
	
	[MenuItem ("Custom/Sound/Toggle 3D sound/Enable")]
	static void Toggle3DSound_Enable() {
		SelectedToggle3DSoundSettings(true);
	}

	static void SelectedToggle3DSoundSettings(bool enabled) {
		
		Object[] audioclips = GetSelectedAudioclips();
		Selection.objects = new Object[0];
		foreach (AudioClip audioclip in audioclips) {
			string path = AssetDatabase.GetAssetPath(audioclip);
			AudioImporter audioImporter = AssetImporter.GetAtPath(path) as AudioImporter;
			audioImporter.threeD = enabled;
			AssetDatabase.ImportAsset(path);
		}
	}

	static Object[] GetSelectedAudioclips()
	{
		return Selection.GetFiltered(typeof(AudioClip), SelectionMode.DeepAssets);
	}
}
