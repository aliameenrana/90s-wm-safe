using UnityEngine;
using System.Collections;
using Prime31;


public class IAPComboUI : MonoBehaviourGUI
{
#if UNITY_IPHONE || UNITY_ANDROID
	void OnGUI()
	{
		beginColumn();

		if( GUILayout.Button( "Init IAP System" ) )
		{
			var key = "your public key from the Android developer portal here";
			key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjuVxxDcCtPyZLhGNB9qNK02pVRfAH43OVo5sr3thKoS4FQKAeZjujrPNFQQO5eeXtDaJ/DDlMOKEyf+qcq24V2MuT+N6riPRIO2ivWUsOFoWV90G1DNh9o4vzkH2cmPujFtGvGRyXAiBKbptz+5HYjnDhtZPRDWFkZ5ctcD+dQZR6Z6HVl0BFA0g2FGEjpzzEAcUkDw2YrSk5OBNcwjYj9wTcWeht1EaE/jPsqETWzWwKTIRVZCI1HrAnHWE1Mwhf/HR50AOpVaLB4YzFwZ2CBt7dCWAENnjfMqd0BaN18rHgtSiPHmifPEjAHNEw0YA9yjkL36mG+eUClphB1jitwIDAQAB";
			IAP.init( key );
		}


		if( GUILayout.Button( "Request Product Data" ) )
		{
			var androidSkus = new string[] { "com.prime31.testproduct", "android.test.purchased", "android.test.purchased2", "com.prime31.managedproduct", "com.prime31.testsubscription" };
			var iosProductIds = new string[] { "anotherProduct", "tt", "testProduct", "sevenDays", "oneMonthSubsciber" };

			IAP.requestProductData( iosProductIds, androidSkus, productList =>
			{
				Debug.Log( "Product list received" );
				Utils.logObject( productList );
			});
		}


		if( GUILayout.Button( "Restore Transactions (iOS only)" ) )
		{
			IAP.restoreCompletedTransactions( productId =>
			{
				Debug.Log( "restored purchased product: " + productId );
			});
		}


		if( GUILayout.Button( "Purchase Consumable" ) )
		{
#if UNITY_ANDROID
		var productId = "android.test.purchased";
#elif UNITY_IPHONE
		var productId = "testProduct";
#endif
			IAP.purchaseConsumableProduct( productId, ( didSucceed, error ) =>
			{
				Debug.Log( "purchasing product " + productId + " result: " + didSucceed );
				
				if( !didSucceed )
					Debug.Log( "purchase error: " + error );
			});
		}


		if( GUILayout.Button( "Purchase Non-Consumable" ) )
		{
#if UNITY_ANDROID
		var productId = "android.test.purchased2";
#elif UNITY_IPHONE
		var productId = "tt";
#endif
			IAP.purchaseNonconsumableProduct( productId, ( didSucceed, error ) =>
			{
				Debug.Log( "purchasing product " + productId + " result: " + didSucceed );
				
				if( !didSucceed )
					Debug.Log( "purchase error: " + error );
			});
		}

		endColumn();
	}
#endif
}
