﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScript : MonoBehaviour {

	public Image ratingsImage;
	public Image fedImage;
	public Image comImage;

	public Sprite noFedSprite;
	public Sprite noComSprite;

	private int lastShowCount;

	public void Awake(){
		lastShowCount = 0;

		SetAverageImage();
	}

	public void SetAverageImage(){
		float averageRating = 0f;
		
		if (GamePlaySession.instance != null)
			averageRating = GamePlaySession.instance.GetAverageStarRating();

		ratingsImage.fillAmount = averageRating;
	}

	public void SetFedImage(){
		int cf = GamePlaySession.instance.selectedFederation;
		if (cf >= 0) {
			fedImage.sprite = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Federations[cf].littleSprite;
		} else {
			fedImage.sprite = noFedSprite;
		}
	}

	public void SetCommishImage(){
		int cc = GamePlaySession.instance.selectedComissioner;
		if (cc >= 0) {
			comImage.sprite = GameObject.Find("UIManager").GetComponent<GameSetupItems>().Commisioners[cc].littleSprite;
		} else {
			comImage.sprite = noComSprite;
		}
	}

	public void UpdateHUD(){
        BroadcastMessage("ShowMyCash",SendMessageOptions.DontRequireReceiver);
        BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
        
		if (GamePlaySession.instance.myShows.Count != lastShowCount) {
			SetAverageImage();
			lastShowCount = GamePlaySession.instance.myShows.Count;
		}
	}
}
