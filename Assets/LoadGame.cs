﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class LoadGame : MonoBehaviour {

	public Text noticeText;
	public FlashText flash;
	public GameObject MainCanvas;
	public GameObject UIManager;
	public GameObject SocialManager;
    public GameObject LoadingBar;
    public List<GameObject> LoadingDots;

	int versionNumber = 1;
	int loadCheck1 = 0, loadCheck2 = 0, loadCheck3 = 0, loadCheck4 = 0;
	string url;
	bool serverWait = false;

	GameSetupItems gameSetupItems;
	UIManagerScript UIManagerScript;
	FaceBookManager FBManager;

	// Use this for initialization
	IEnumerator Start()
	{
		//Umar:: For Testing
//		PlayerPrefs.SetString("UserID", "UIJHOC41R8NHWT7A");

		gameSetupItems = UIManager.GetComponent<GameSetupItems>();
		UIManagerScript = UIManager.GetComponent<UIManagerScript>();
		FBManager = SocialManager.GetComponent<FaceBookManager>();

		if(PlayerPrefs.GetString("UserID", "") != "")
		{
			serverWait = true;
			//Debug.Log("Umar:: Sending playfab login call.");
			//Debug.Log("Umar:: UserID : " + PlayerPrefs.GetString("UserID"));
			PlayFabManager.GetInstance().Login(PlayerPrefs.GetString("UserID"));
			StartCoroutine(CheckPFLogin());
			url = "https://s3-ap-southeast-1.amazonaws.com/90wm/";
		}
		else //user id not set means its first login so load assets from streaming assets
		{
            #if UNITY_EDITOR
            url = "https://s3-ap-southeast-1.amazonaws.com/90wm/";
            #else
            url = Application.streamingAssetsPath + "/";
            #endif
		}

		//Debug.Log(url);

		LoadAssets();

		while((loadCheck1+loadCheck2+loadCheck3+loadCheck4)<4)
		{
			//Debug.Log("Resources Loaded : " + (loadCheck1+loadCheck2+loadCheck3+loadCheck4).ToString() + "/4");
			yield return new WaitForSeconds(0.5f);
		}

		while(serverWait)
		{
			Debug.Log("Waiting for server");
			yield return new WaitForSeconds(0.5f);
		}

		ActivateObjects();
	}

	void ActivateObjects()
	{
		UIManager.SetActive(true);
		MainCanvas.SetActive(true);
		flash.waiting = false;
		gameObject.SetActive(false);
	}

	IEnumerator CheckPFLogin()
	{
		while(!PlayFabManager.GetInstance().IsPFLoggedIn())
			yield return null;
		
		serverWait = false;
	}

	void LoadAssets()
	{
		StartCoroutine(DownloadAndCache("xmlresources", versionNumber));
		StartCoroutine(DownloadAndCache("cardsback", versionNumber));
		StartCoroutine(DownloadAndCache("audio", versionNumber));
		StartCoroutine(DownloadAndCache("cardsfront", versionNumber));
	}
		
	IEnumerator DownloadAndCache (string bundleName, int version)
	{
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;
		
		string urlCurr = url + bundleName;

		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using(WWW www = WWW.LoadFromCacheOrDownload(urlCurr, version))
		{
            yield return www;
            if (www.error != null)
			{
				throw new Exception("WWW download had an error:" + www.error);
			}

			AssetBundle bundle = www.assetBundle;
			//Debug.Log("Umar:: Loaded Asset Bundle : " + bundle.name);

			if(bundleName == "cardsback")
			{
				var data = bundle.LoadAllAssets<Sprite>();

				gameSetupItems.cardLogos = new List<Sprite>();
				gameSetupItems.cardHeadshots = new List<Sprite>();

				gameSetupItems.cardLogoDict = new Dictionary<int, Sprite>();
				gameSetupItems.cardHeadshotDict = new Dictionary<int, Sprite>();

				foreach(Sprite spr in data)
				{
					if(spr.name.Contains("logo"))
					{
						gameSetupItems.cardLogos.Add(spr);
						gameSetupItems.cardLogoDict.Add(int.Parse(Regex.Replace(spr.name, "[^0-9]", "")), spr);
					}
					else
					{
						gameSetupItems.cardHeadshots.Add(spr);
						gameSetupItems.cardHeadshotDict.Add(int.Parse(Regex.Replace(spr.name, "[^0-9]", "")), spr);
					}
				}
				loadCheck1++;
			}
			else if(bundleName == "audio")
			{
				var data = bundle.LoadAllAssets<AudioClip>();

				gameSetupItems.cardAudio = new List<AudioClip>();
				gameSetupItems.cardAudioDict = new Dictionary<int, AudioClip>();

				foreach(AudioClip audio in data)
				{
					gameSetupItems.cardAudio.Add(audio);
					gameSetupItems.cardAudioDict.Add(int.Parse(Regex.Replace(audio.name, "[^0-9]", "")), audio);
				}
				loadCheck2++;
			}
			else if(bundleName == "cardsfront")
			{
				var data = bundle.LoadAllAssets<Sprite>();

				gameSetupItems.cardFronts = new List<Sprite>();
				gameSetupItems.cardFrontDict = new Dictionary<int, Sprite>();

				int i=0;
				foreach(Sprite spr in data)
				{
					gameSetupItems.cardFronts.Add(spr);
					gameSetupItems.cardFrontDict.Add(int.Parse(Regex.Replace(spr.name, "[^0-9]", "")), spr);
				}
				loadCheck3++;

			}
			else if(bundleName == "xmlresources")
			{
				gameSetupItems.wrestlerDoc = bundle.LoadAsset<TextAsset>("wrestler.xml").text;
				gameSetupItems.venuesDoc = bundle.LoadAsset<TextAsset>("venues.xml").text;
				gameSetupItems.flavorsDoc = bundle.LoadAsset<TextAsset>("flavors.xml").text;
				gameSetupItems.matchesDoc = bundle.LoadAsset<TextAsset>("matches.xml").text;
				gameSetupItems.micSpotsDoc = bundle.LoadAsset<TextAsset>("micSpots.xml").text;
				gameSetupItems.skitsDoc = bundle.LoadAsset<TextAsset>("skits.xml").text;
				gameSetupItems.nykDoc = bundle.LoadAsset<TextAsset>("nowyouknow.xml").text;
				loadCheck4++;
			}

			// Unload the AssetBundles compressed contents to conserve memory
			bundle.Unload(false);
		}// memory is freed from the web stream (www.Dispose() gets called implicitly)
	}
}
