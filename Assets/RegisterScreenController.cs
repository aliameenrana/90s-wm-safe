﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class RegisterScreenController : MonoBehaviour
{
    public InputField RegisterEmail,RegisterPassword,RegisterPasswordRepeat;
    public Text message;
    public GameObject UIManager, socialManager;
    FaceBookManager FBManager;
    /// <summary>
	/// Regular expression, which is used to validate an E-Mail address.
	/// </summary>
	const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

    /// <summary>
    /// Checks whether the given Email-Parameter is a valid E-Mail address.
    /// </summary>
    /// <param name="email">Parameter-string that contains an E-Mail address.</param>
    /// <returns>True, when Parameter-string is not null and 
    /// contains a valid E-Mail address;
    /// otherwise false.</returns>
    bool IsEmail(string email)
    {
        if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }

    void Awake()
    {
        UIManager = GameObject.Find("UIManager");
        FBManager = socialManager.GetComponent<FaceBookManager>();
    }
    void ShowToast(string text)
    {
        StartCoroutine(ShowToastCR(text));
    }

    IEnumerator ShowToastCR(string text)
    {
        message.gameObject.SetActive(true);
        message.text = text;
        yield return new WaitForSeconds(3f);
        message.gameObject.SetActive(false);
        if (text == "User successfully registered." || text == "User already exists...")
        {
            if (ScreenHandler.current != null && UIManager.GetComponent<UIManagerScript>().LoginScreen != null)
            {
                ScreenHandler.current.OpenPanel(UIManager.GetComponent<UIManagerScript>().LoginScreen);
            }
        }
    }

    public void Register()
    {
        if (!string.IsNullOrEmpty(RegisterEmail.text) && !string.IsNullOrEmpty(RegisterPassword.text) && !string.IsNullOrEmpty(RegisterPasswordRepeat.text))
        {
            if (RegisterPassword.text == RegisterPasswordRepeat.text)
            {
                if (!IsEmail(RegisterEmail.text))
                {
                    ShowToast("Please enter valid email address");
                }
                else if (RegisterPassword.text.Length < 8)
                {
                    ShowToast("Password should be atleast 8 characters long.");
                }
                else
                {
                    StartCoroutine(SendRequest("REGISTER"));
                }
            }
            else
            {
                ShowToast("Passwords don't match");
            }
        }
            
    }

    private IEnumerator SendRequest(string reqType)
    {
        Debug.Log("REQTYPE: "+reqType);
        Debug.Log(RegisterEmail.text + RegisterPassword.text+ FBManager.GetFBAccessToken().ToString());
        
        string url = "http://bugdevstudios.com/wrestling/bk/server.php?REQUEST=" + reqType + "&ACCESS_TOKEN=" + "RandomToken" + "&EMAIL=" + RegisterEmail.text + "&PASSWORD=" + RegisterPassword.text;
        Debug.Log(url);
        //Debug.Log(url);
        WWW www = new WWW(url);

        yield return www;
        
        if (www.error != null)
        {
            
            
            yield break;
        }
        
        //Debug.Log(www.text);
        ProcessResponse(www.text);

        www.Dispose();
    }

    void ProcessResponse(string response)
    {
        Debug.Log("RESPONSE IS: "+response.ToString());

        var N = JSON.Parse(response);
        //		var statusString = N["STATUS"].Value;
        var responseCode = N["CODE"].AsInt;
        //var userID = N["USER_ID"].Value;

        if (responseCode > 0)
        {
            if (responseCode == 1)
            {
                ShowToast("User successfully registered.");
            }
        }
        else
        {
            if (responseCode == -8)
            {
                ShowToast("User already exists...");
            }
        }
    }
}
