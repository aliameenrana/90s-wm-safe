﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsScreenController : MonoBehaviour {

    public GameObject UIManager, WLRecordButton, TitlesButton, AwardsButton, WLHeads,WLRecords,TitlesHeads,TitlesRecords, AwardHeads, AwardRecords, WLRecordLine,TitleRecordLine, World;
    public Button StatsBackButton;
    public Text[] PlaceHolders;
    public GameObject[] AwardHolderLogos;
    int currentStatMenu, TitleRecordSelected = 1;
    int[] SelectableYears = new int[20] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
    int SelectedYear = 0;

    void Awake()
    {
        UIManager = GameObject.Find("UIManager");
    }

    public void OpenWLRecords()
    {
        WLRecordButton.SetActive(false);
        TitlesButton.SetActive(false);
        AwardsButton.SetActive(false);
        WLHeads.SetActive(true);
        WLRecords.SetActive(true);
        PopulateWLRecordsPanel(0);
        currentStatMenu = 0;
        StatsBackButton.onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        StatsBackButton.onClick.AddListener(() => CloseCurrentStatsMenu());
    }

    public void OpenTitlesRecords()
    {
        WLRecordButton.SetActive(false);
        TitlesButton.SetActive(false);
        AwardsButton.SetActive(false);
        TitlesHeads.SetActive(true);
        TitlesRecords.SetActive(true);
        PopulateTitlesRecordsPanel(World);
        currentStatMenu = 1;
        StatsBackButton.onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        StatsBackButton.onClick.AddListener(() => CloseCurrentStatsMenu());
    }

    public void OpenAwards()
    {
        WLRecordButton.SetActive(false);
        TitlesButton.SetActive(false);
        AwardsButton.SetActive(false);
        AwardHeads.SetActive(true);
        AwardRecords.SetActive(true);
        currentStatMenu = 2;
        StatsBackButton.onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        StatsBackButton.onClick.AddListener(() => CloseCurrentStatsMenu());
        
        int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
        SelectedYear = ThisYear - 1980;
        PopulateAwardsPanel(SelectedYear);
        //AwardYearLabel.text = "Year: "+SelectedYear + 1980.ToString();
        
    }

    public void IncrementYearAward()
    {
        SelectedYear++;
        if (SelectedYear >= 20)
        {
            SelectedYear = 0;
        }
        PopulateAwardsPanel(SelectedYear);
    }
    public void IncrementYearWL()
    {
        SelectedYear++;
        if (SelectedYear >= 20)
        {
            SelectedYear = 0;
        }
        PopulateWLRecordsPanel(SelectedYear);
    }

    public void PopulateWLRecordsPanel(int Year)
    {
        WLHeads.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = (Year + 1980).ToString();
        foreach (Transform item in WLRecords.transform.GetChild(0).transform.GetChild(0).transform)
        {
            item.gameObject.SetActive(false);
        }

        foreach (CardObject item in GamePlaySession.instance.myCards)
        {
            if (item.myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject wrestler = (WrestlerCardObject)item;
                GameObject WLFigures = WLRecords.transform.GetChild(0).transform.GetChild(0).transform.gameObject;
                GameObject WLRLine = Instantiate(WLRecordLine, WLFigures.transform);
                WLRLine.transform.localScale = new Vector3(1,1,1);
                foreach (Transform item2 in WLRLine.transform)
                {
                    if (item2.name == "WrestlerName")
                    {
                        item2.gameObject.GetComponent<Text>().text = wrestler.name;
                    }
                    else if (item2.name == "Wins")
                    {
                        item2.gameObject.GetComponent<Text>().text = wrestler.WinsCurrent[Year].ToString();
                    }
                    else if (item2.name == "Losses")
                    {
                        item2.gameObject.GetComponent<Text>().text = wrestler.LossesCurrent[Year].ToString();
                    }
                }
            }
        }
    }

    public void PopulateTitlesRecordsPanel(GameObject selector)
    {
        foreach (Transform item in TitlesRecords.transform.GetChild(0).transform.GetChild(0).transform)
        {
            if (item.name.ToUpper() != "HEADERS")
            {
                item.gameObject.SetActive(false);
            }
        }
        foreach (CardObject item in GamePlaySession.instance.myCards)
        {
            if (item.myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject wrestler = (WrestlerCardObject)item;

                if (selector.name.ToUpper() == "TAG")
                {
                    if (GamePlaySession.instance.AllTagChampions.Count > 0)
                    {
                        foreach (var item2 in GamePlaySession.instance.AllTagChampions)
                        {
                            if (item2.CardNumber == wrestler.CardNumber)
                            {
                                GameObject TitleFigures = TitlesRecords.transform.GetChild(0).transform.GetChild(0).transform.gameObject;
                                GameObject TitlesLine = Instantiate(TitleRecordLine, TitleFigures.transform);
                                TitlesLine.transform.localScale = new Vector3(1, 1, 1);
                                foreach (Transform item3 in TitlesLine.transform)
                                {
                                    if (item3.name == "WrestlerName")
                                    {
                                        if (wrestler.name!=null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.name;
                                    }
                                    else if (item3.name == "Won on")
                                    {
                                        if (wrestler.CrowningDate != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.CrowningDate.ToString();
                                        else if (wrestler.CrowningDate == null)
                                        {
                                            item3.gameObject.GetComponent<Text>().text = "N/A";
                                        }
                                    }
                                    else if (item3.name == "Defenses")
                                    {
                                        if(wrestler.Defenses!=null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.Defenses.ToString();
                                        else if (wrestler.Defenses == null)
                                        {
                                            item3.gameObject.GetComponent<Text>().text = "N/A";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (selector.name.ToUpper() == "WORLD")
                {
                    if (GamePlaySession.instance.AllWorldChampions.Count > 0)
                    {
                        foreach (var item2 in GamePlaySession.instance.AllWorldChampions)
                        {
                            if (item2.CardNumber == wrestler.CardNumber)
                            {
                                GameObject TitleFigures = TitlesRecords.transform.GetChild(0).transform.GetChild(0).transform.gameObject;
                                GameObject TitlesLine = Instantiate(TitleRecordLine, TitleFigures.transform);
                                TitlesLine.transform.localScale = new Vector3(1, 1, 1);
                                foreach (Transform item3 in TitlesLine.transform)
                                {
                                    if (item3.name == "WrestlerName")
                                    {
                                        if (wrestler.name != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.name;
                                    }
                                    else if (item3.name == "Won on")
                                    {
                                        if (wrestler.CrowningDate != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.CrowningDate.ToString();
                                        else if (wrestler.CrowningDate == null)
                                        {
                                            item3.gameObject.GetComponent<Text>().text = "N/A";
                                        }
                                    }
                                    else if (item3.name == "Defenses")
                                    {
                                        if (wrestler.Defenses != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.Defenses.ToString();
                                        else if (wrestler.Defenses == null)
                                        {
                                            item3.gameObject.GetComponent<Text>().text = "N/A";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (selector.name.ToUpper() == "TV")
                {
                    if (GamePlaySession.instance.AllTitleChampions.Count > 0)
                    {
                        foreach (var item2 in GamePlaySession.instance.AllTitleChampions)
                        {
                            if (item2.CardNumber == wrestler.CardNumber)
                            {
                                GameObject TitleFigures = TitlesRecords.transform.GetChild(0).transform.GetChild(0).transform.gameObject;
                                GameObject TitlesLine = Instantiate(TitleRecordLine, TitleFigures.transform);
                                TitlesLine.transform.localScale = new Vector3(1, 1, 1);
                                foreach (Transform item3 in TitlesLine.transform)
                                {
                                    if (item3.name == "WrestlerName")
                                    {
                                        if (wrestler.name != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.name;
                                    }
                                    else if (item3.name == "Won on")
                                    {
                                        if (wrestler.CrowningDate != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.CrowningDate.ToString();
                                        else if (wrestler.CrowningDate == null)
                                        {
                                            item3.gameObject.GetComponent<Text>().text = "N/A";
                                        }
                                    }
                                    else if (item3.name == "Defenses")
                                    {
                                        if (wrestler.Defenses != null)
                                            item3.gameObject.GetComponent<Text>().text = wrestler.Defenses.ToString();
                                        else if (wrestler.Defenses == null)
                                        {
                                            item3.gameObject.GetComponent<Text>().text = "N/A";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void PopulateAwardsPanel(int Year)
    {
        AwardHeads.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = (Year + 1980).ToString();
        if (GamePlaySession.instance.Awards != null)
        {
            if (GamePlaySession.instance.Awards[SelectedYear] == null || GamePlaySession.instance.Awards[SelectedYear].Count == 0)
            {
                foreach (Text item in PlaceHolders)
                {
                    item.gameObject.SetActive(true);
                }
            }
            else if (GamePlaySession.instance.Awards[SelectedYear] != null && GamePlaySession.instance.Awards[SelectedYear].Count > 0)
            {
                foreach (Text item in PlaceHolders)
                {
                    item.gameObject.SetActive(false);
                }
                CardObject[] C = new CardObject[4] { GamePlaySession.instance.Awards[SelectedYear][0], GamePlaySession.instance.Awards[SelectedYear][1], GamePlaySession.instance.Awards[SelectedYear][2], GamePlaySession.instance.Awards[SelectedYear][3] };
                AwardHolderLogos[0].GetComponent<Image>().sprite = GamePlaySession.instance.GSI.cardLogoDict[C[0].CardNumber];
                AwardHolderLogos[1].GetComponent<Image>().sprite = GamePlaySession.instance.GSI.cardLogoDict[C[0].CardNumber];
                AwardHolderLogos[2].GetComponent<Image>().sprite = GamePlaySession.instance.GSI.cardLogoDict[C[0].CardNumber];
                AwardHolderLogos[3].GetComponent<Image>().sprite = GamePlaySession.instance.GSI.cardLogoDict[C[0].CardNumber];
            }
        }
        else if (GamePlaySession.instance.Awards == null)
        {

        }
    }

    public void CloseCurrentStatsMenu()
    {
        switch (currentStatMenu)
        {
            case 0:
                WLHeads.SetActive(false);
                WLRecords.SetActive(false);
                break;
            case 1:
                TitlesHeads.SetActive(false);
                TitlesRecords.SetActive(false);
                break;
            case 2:
                AwardRecords.SetActive(false);
                AwardHeads.SetActive(false);
                break;
        }
        StatsBackButton.onClick.RemoveAllListeners();
        StatsBackButton.onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        WLRecordButton.SetActive(true);
        TitlesButton.SetActive(true);
        AwardsButton.SetActive(true);
    }

    public void SwitchTitlesRecords(GameObject selector)
    {
        PopulateTitlesRecordsPanel(selector);
    }
}
