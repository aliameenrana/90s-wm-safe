﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class UIManagerScript : MonoBehaviour
{

    public Animator TitleScreen;
    public Animator LoginScreen;
    public Animator DisclaimerScreen;
    public Animator WelcomeScreen;
    public Animator FedScreen;
    public Animator FedScreenVerify;

    public Animator MyFedScreen;
    public Animator CommishSelectScreen;
    public Animator CommishScreenVerify;

    public Animator MainCalendarScreen;
    public Animator NowYouKnowScreen;
    public Animator ShowSetupScreen;
    public Animator MatchSetupScreen;
    public Animator TagMatchSetupScreen;
    public Animator MicSpotSetupScreen;
    public Animator SkitSetupScreen;
    public Animator ShowTransitionScreen;
    public Animator ShowResultsScreen;
    public Animator CantPromoteChampScreen;
    public Animator CantDemoteChampScreen;
    public Animator DemoteVerifyScreen;
    public Animator StatsSelectScreen;
    public Animator CheckListScreen;
    public Animator PromoteVerifyScreen;
    public Animator RegisterScreen;

    public Animator ShopScreen;

    public Animator HelpScreen;
    public Animator CreditsScreen;
    public Animator StartYearSelect;
    public Animator AwardsGivingScreen;

    public EndGameScreen endGameScreen;

    public PopUpWindow popUpWindow;

    public CardSelectScreenScript cardViewer;

    public GameObject PlayerHUD;
    public bool sfxEnabled;

    public AudioSource buttonSoundSource;

    public GameObject screenNode;
    public GameObject SelectedYear;
    public GameObject CheckListLinePrefab, CheckListActiveType;
    public Sprite CheckedCheckBox;
    public Sprite UnCheckedCheckBox;

    public static UIManagerScript instance;
    public string myStr = "";
    public float T;
    public List<GameObject> CheckListLines;


    CardType ActiveCheckListType = new CardType();

    public UIManagerScript GetInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
        return instance;
    }

    void Awake()
    {
        instance = this;
        T = Time.time;
    }

    void Update()
    {
        if (screenNode != null && !screenNode.activeInHierarchy)
        {
            screenNode.SetActive(true);
            screenNode.transform.parent.gameObject.SetActive(true);

            if (ScreenHandler.current != null)
                ScreenHandler.current.RefreshPanel();
        }
        if (Time.time - T > 15)
        {
            Resources.UnloadUnusedAssets();
            T = Time.time;
        }
    }

    public void NewGame()
    {
        GamePlaySession.instance = new GamePlaySession();
        GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
        // Add any code needed to flag the player as beginning a new game!
        if (ScreenHandler.current != null && DisclaimerScreen != null)
            ScreenHandler.current.OpenPanel(DisclaimerScreen);
    }

    public void NewGamePlus()
    {
        // Add any code needed to flag the player as beginning a new game+!
        GamePlaySession newGamePlus = new GamePlaySession();

        // Copy over cards, cash, and tokens
        newGamePlus.myCards = GamePlaySession.instance.myCards;
        newGamePlus.myCash = Mathf.Max(GamePlaySession.instance.myCash, 0);
        newGamePlus.myTokens = GamePlaySession.instance.myTokens;

        // Ensure the store doesn't reset itself
        newGamePlus.firstShopCards = true;
        newGamePlus.storeCards = GamePlaySession.instance.storeCards;
        newGamePlus.storeCardsBought = GamePlaySession.instance.storeCardsBought;
        newGamePlus.storePurchases = GamePlaySession.instance.storePurchases;
        newGamePlus.lastShopRefresh = GamePlaySession.instance.lastShopRefresh;

        // Set as a new game plus playthrough
        newGamePlus.newGamePlus = true;

        GamePlaySession.instance = newGamePlus;
        GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
        if (ScreenHandler.current != null && DisclaimerScreen != null)
            ScreenHandler.current.OpenPanel(DisclaimerScreen);
    }

    public void ContinueGame()
    {
        // Add any code needed to flag the player as continuing an existing game!
        //Update the HUD and turn it on where appropriate
        if (GamePlaySession.instance.selectedFederation == -1)
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
                ScreenHandler.current.OpenPanel(FedScreen);
            return;
        }

        if (GamePlaySession.instance.selectedComissioner == -1)
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && CommishSelectScreen != null)
                ScreenHandler.current.OpenPanel(CommishSelectScreen);
            return;
        }

        //StartMyFed();

        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void StartGame()
    {
        if (GamePlaySession.instance.isNewPlayer && !GamePlaySession.instance.newGamePlus)
        {
            // If new player, head to the welcome screen and begin the tutorial!
            if (ScreenHandler.current != null && WelcomeScreen != null)
                ScreenHandler.current.OpenPanel(WelcomeScreen);

            GamePlaySession.instance.myCash = 10000;
            GamePlaySession.instance.myTokens = 1;
            GamePlaySession.instance.isNewPlayer = false;
        }
        else
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
            {
                ScreenHandler.current.OpenPanel(FedScreen);
                FedScreen.transform.GetChild(1).gameObject.SetActive(true);
                //Debug.Log(FedScreen.transform.GetChild(1).gameObject.name);
            }
        }

        PlayerHUD.GetComponent<HUDScript>().SetFedImage();
        PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
        PlayerHUD.GetComponent<HUDScript>().UpdateHUD();

    }

    public void ShowHUD()
    {
        PlayerHUD.SetActive(true);
        UpdateHUD();
    }

    public void UpdateHUD()
    {
        PlayerHUD.BroadcastMessage("ShowMyCash", SendMessageOptions.DontRequireReceiver);
        PlayerHUD.BroadcastMessage("ShowMyTokens", SendMessageOptions.DontRequireReceiver);
    }

    public void VerifyFed()
    {
        if (ScreenHandler.current != null && FedScreenVerify != null)
            ScreenHandler.current.OpenPanel(FedScreenVerify);
    }
    public void VerifyCommish()
    {
        if (ScreenHandler.current != null && CommishScreenVerify != null)
            ScreenHandler.current.OpenPanel(CommishScreenVerify);
    }

    public void StartMyFed()
    {
        if (MyFedScreen == null)
            return;

        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void GoToMyFed()
    {
        // Should do a check to see if we can go to MyFed screen from current place in game...
        Animator currentPanel = null;
        if (ScreenHandler.current != null)
            currentPanel = ScreenHandler.current.GetCurrentPanel();

        if (currentPanel != MainCalendarScreen)
            return;

        if (MyFedScreen == null)
            return;
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
        //StartCoroutine(SaveGamePlaySession());
        //OnSaveAll();
    }

    public void HomeButton()
    {
        if (MyFedScreen == null)
            return;
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
        //StartCoroutine(SaveGamePlaySession());
        //OnSaveAll();
    }

    public void GoToCalendar(bool UpdateRatings)
    {
        if (MainCalendarScreen == null)
            return;
        CalendarScreen calScreen = MainCalendarScreen.GetComponent<CalendarScreen>();
        calScreen.OpenCalendar(UpdateRatings);

        if (!GamePlaySession.instance.calendarTutorialClear && !GamePlaySession.instance.newGamePlus)
        {
            GamePlaySession.instance.calendarTutorialClear = true;

            PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

            if (popUpWindow != null)
            {
                popUpWindow.OpenPopUpWindow(new PopUpMessage[] { calScreen.initialPopUp }, MainCalendarScreen);
            }
        }
        //StartCoroutine(SaveGamePlaySession());
        //OnSaveAll();
    }

    public void GoToShop()
    {
        if (GamePlaySession.instance == null)
            return;

        if (ShopScreen != null)
        {

            StorePage storePage = ShopScreen.gameObject.GetComponent<StorePage>();

            if (storePage != null)
            {
                //Debug.Log("Refreshing Card Display..");
                storePage.RefreshCardDisplay();
            }


            if (ScreenHandler.current != null)
            {
                PlayerHUD.SetActive(false);
                ScreenHandler.current.OpenPanel(ShopScreen);
            }
            //StartCoroutine(SaveGamePlaySession());
            //OnSaveAll();
        }
    }

    public void GoToTitle()
    {
        if (ScreenHandler.current != null)
        {
            PlayerHUD.SetActive(false);
            ScreenHandler.current.OpenPanel(TitleScreen);
        }
    }

    public void GoToCardView()
    {
        if (cardViewer == null)
            return;
        cardViewer.OpenCardView();
        //StartCoroutine(SaveGamePlaySession());
    }

    public void Start()
    {
        if (PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE") != "NONE")
        {
            GamePlaySession.instance = GamePlaySession.Load(PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE"));
            GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
            PlayerHUD.GetComponent<HUDScript>().SetFedImage();
            PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
            PlayerHUD.GetComponent<HUDScript>().UpdateHUD();
        }
        else
        {
            //Debug.Log("Building new Gameplay Session!");
            GamePlaySession.instance = new GamePlaySession();
        }

        CloseAllPanels();
    }

    void OnSaveAll()
    {
        if (GamePlaySession.instance != null && GamePlaySession.instance.myCards.Count != 0)
            GamePlaySession.instance.SaveAll();
    }

    public void OpenYearSelect()
    {
        //PlayerHUD.SetActive(true);
        if (ScreenHandler.current != null && StartYearSelect != null)
            ScreenHandler.current.OpenPanel(StartYearSelect);
    }

    public void SelectYear(GameObject SelectedYear)
    {
        //SelectedYear = EventSystem.current.currentSelectedGameObject;
        for (int i = 0; i < GamePlaySession.instance.numberOfYears; i++)
        {

            if (SelectedYear.name == "Year1980")
            {
                GamePlaySession.instance.myCalendar.years[i].yearName = (1980 + i).ToString();
            }
            else if (SelectedYear.name == "Year1990")
            {
                if (i < 10)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1990 + i).ToString();
                }
                else if (i >= 10)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1980 + i).ToString();
                }
            }
            else if (SelectedYear.name == "Year1985")
            {
                if (i < 15)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1985 + i).ToString();
                }
                else if (i >= 15)
                {
                    GamePlaySession.instance.myCalendar.years[i].yearName = (1985 + i - 20).ToString();
                }
            }
        }
        StartGame();
    }

    void CloseAllPanels()
    {
        if (TitleScreen != null)
            TitleScreen.gameObject.SetActive(false);
        if (LoginScreen != null)
            LoginScreen.gameObject.SetActive(false);
        if (DisclaimerScreen != null)
            DisclaimerScreen.gameObject.SetActive(false);
        if (WelcomeScreen != null)
            WelcomeScreen.gameObject.SetActive(false);
        if (FedScreen != null)
            FedScreen.gameObject.SetActive(false);
        if (FedScreenVerify != null)
            FedScreenVerify.gameObject.SetActive(false);
        if (MyFedScreen != null)
            MyFedScreen.gameObject.SetActive(false);
        if (CommishSelectScreen != null)
            CommishSelectScreen.gameObject.SetActive(false);
        if (CommishScreenVerify != null)
            CommishScreenVerify.gameObject.SetActive(false);
        if (MainCalendarScreen != null)
            MainCalendarScreen.gameObject.SetActive(false);
        if (NowYouKnowScreen != null)
            NowYouKnowScreen.gameObject.SetActive(false);
        if (ShowSetupScreen != null)
            ShowSetupScreen.gameObject.SetActive(false);
        if (MatchSetupScreen != null)
            MatchSetupScreen.gameObject.SetActive(false);
        if (TagMatchSetupScreen != null)
            TagMatchSetupScreen.gameObject.SetActive(false);
        if (MicSpotSetupScreen != null)
            MicSpotSetupScreen.gameObject.SetActive(false);
        if (SkitSetupScreen != null)
            SkitSetupScreen.gameObject.SetActive(false);
        if (ShowTransitionScreen != null)
            ShowTransitionScreen.gameObject.SetActive(false);
        if (ShowResultsScreen != null)
            ShowResultsScreen.gameObject.SetActive(false);
        if (ShopScreen != null)
            ShopScreen.gameObject.SetActive(false);
        if (HelpScreen != null)
            HelpScreen.gameObject.SetActive(false);
        if (CreditsScreen != null)
            CreditsScreen.gameObject.SetActive(false);
    }

    private void OnApplicationQuit()
    {
        //Umar:: Save data locally and upload to server if possible
        string data = myStr;
    }

    public void OpenStatsSelect()
    {
        if (ScreenHandler.current != null && StatsSelectScreen != null)
            ScreenHandler.current.OpenPanel(StatsSelectScreen);

        if (PlayerHUD != null && PlayerHUD.activeSelf == true)
        {
            PlayerHUD.SetActive(false);
        }
    }

    public void OpenCheckListScreen()
    {
        if (ScreenHandler.current != null && CheckListScreen != null)
            ScreenHandler.current.OpenPanel(CheckListScreen);

        if (PlayerHUD != null && PlayerHUD.activeSelf == true)
        {
            PlayerHUD.SetActive(false);
        }
        PopulateCheckListScreen(CardType.WRESTLER);
    }
    public void PopulateCheckListScreen(CardType CardType)
    {
        GameObject CheckListFigures = GameObject.Find("CheckListFigures");
        CheckListFigures.transform.localPosition = new Vector3(CheckListFigures.transform.position.x, 0, CheckListFigures.transform.position.z);
        

        if (CheckListFigures.transform.childCount == 0)
        {
            CheckListLines = new List<GameObject>();
            for (int i = 0; i < 170; i++)
            {
                GameObject CheckListLine = Instantiate(CheckListLinePrefab, CheckListFigures.transform);
                CheckListLine.transform.localScale = new Vector3(1, 1, 1);
                CheckListLines.Add(CheckListLine);
            }
        }

        foreach (GameObject item in CheckListLines)
        {
            foreach (Transform item2 in item.transform)
            {
                if (item2.gameObject.GetComponent<Text>() != null)
                {
                    item2.gameObject.GetComponent<Text>().text = "";
                }
                else if (item2.gameObject.GetComponent<Text>() == null && item2.gameObject.name == "CheckBox")
                {
                    item2.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                }
            }
        }

        
        //foreach (Transform item in CheckListFigures.transform)
        //{
        //    if (item.gameObject.activeSelf == true)
        //    {
        //        item.gameObject.SetActive(false);
        //    }
        //}
        int CardNumber = 1;

        List<CardObject> CardsList = new List<CardObject>();
        bool Wrestler = false;


        if ((CardType == CardType.ALL) || (CardType == CardType.SHOP_COMMON) || (CardType == CardType.SHOP_RARE) || (CardType == CardType.SHOP_UNCOMMON))
        {
            
        }
        else if (CardType == CardType.WRESTLER)
        {
            foreach (CardObject item in GameSetupItems.instance.wrestlers.cards.Values)
            {
                if (item.myCardType == CardType.WRESTLER)
                {
                    Wrestler = true;
                    WrestlerCardObject wrestler = (WrestlerCardObject)item;
                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                    {
                        //GameObject CheckListLine = Instantiate(CheckListLinePrefab, CheckListFigures.transform);
                        //CheckListLine.transform.localScale = new Vector3(1, 1, 1);
                        bool found = false;
                        foreach (CardObject item4 in GamePlaySession.instance.myCards)
                        {
                            if (item4.CardNumber == item.CardNumber)
                            {
                                found = true;
                            }
                        }
                        foreach (Transform item3 in CheckListLines[CardNumber-1].transform)
                        {
                            if (item3.name == "CardNumber")
                            {
                                item3.gameObject.GetComponent<Text>().text = CardNumber.ToString();
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "CardName")
                            {
                                item3.gameObject.GetComponent<Text>().text = wrestler.name.ToString();
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "Rarity")
                            {
                                item3.gameObject.GetComponent<Text>().text = wrestler.rarity.ToString();
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "Type")
                            {
                                item3.gameObject.GetComponent<Text>().text = "Wrestler";
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.black;
                                }
                                else if (found == false)
                                {
                                    item3.gameObject.GetComponent<Text>().color = Color.white;
                                }
                            }
                            else if (item3.name == "CheckBox")
                            {
                                if (found == true)
                                {
                                    item3.gameObject.GetComponent<Image>().sprite = CheckedCheckBox;
                                }
                                else if (found == true)
                                {
                                    item3.gameObject.GetComponent<Image>().sprite = UnCheckedCheckBox;
                                }
                            }
                        }
                        CardNumber++;
                    }
                }
            }
        }

        else if ((CardType == CardType.FEUD) || (CardType == CardType.SPONSOR) || (CardType == CardType.TAG_TEAM) || (CardType == CardType.MANAGER) || (CardType == CardType.MERCH))
        {
            foreach (CardObject item in GameSetupItems.instance.flavors.cards.Values)
            {
                FlavorCardObject flavor = (FlavorCardObject)item;
                if ((CardType == CardType.FEUD && flavor.cardClass == "Feud") || (CardType == CardType.SPONSOR && flavor.cardClass == "Sponsor") || (CardType == CardType.TAG_TEAM && flavor.cardClass == "Tag Team") || (CardType == CardType.MANAGER && flavor.cardClass == "Manager") || (CardType == CardType.MERCH && flavor.cardClass == "Merchandise"))
                {
                    CardsList.Add(item);
                }
            }
        }
        else if ((CardType == CardType.FLAVOR))
        {
            foreach (CardObject item in GameSetupItems.instance.flavors.cards.Values)
            {
                FlavorCardObject flavor = (FlavorCardObject)item;
                if ((flavor.cardClass != "Merchandise") && (flavor.cardClass != "Manager") && (flavor.cardClass != "Sponsor") && (flavor.cardClass != "Feud") && (flavor.cardClass != "Tag Team"))
                {
                    CardsList.Add(item);
                }
            }
        }
        else if (CardType == CardType.MATCH)
        {
            foreach (CardObject item in GameSetupItems.instance.matches.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else if (CardType == CardType.MIC_SPOT)
        {
            foreach (CardObject item in GameSetupItems.instance.micSpots.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else if (CardType == CardType.SKIT)
        {
            foreach (CardObject item in GameSetupItems.instance.skits.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else if (CardType == CardType.VENUE)
        {
            foreach (CardObject item in GameSetupItems.instance.venues.cards.Values)
            {
                CardsList.Add(item);
            }
        }
        else
        {
            Debug.Log("Invalid Card type selected");
        }

        if (Wrestler == false)
        {
            foreach (CardObject item in CardsList)
            {
                //GameObject CheckListLine = Instantiate(CheckListLinePrefab, CheckListFigures.transform);
                //CheckListLine.transform.localScale = new Vector3(1, 1, 1);
                bool found = false;
                foreach (CardObject itemx in GamePlaySession.instance.myCards)
                {
                    if (itemx.CardNumber == item.CardNumber)
                    {
                        found = true;
                    }
                    foreach (Transform item3 in CheckListLines[CardNumber-1].transform)
                    {
                        if (item3.name == "CardNumber")
                        {
                            item3.gameObject.GetComponent<Text>().text = CardNumber.ToString();
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "CardName")
                        {
                            if (item.myCardType == CardType.FLAVOR || item.myCardType == CardType.FEUD || item.myCardType == CardType.MANAGER || item.myCardType == CardType.SPONSOR || item.myCardType == CardType.TAG_TEAM || item.myCardType == CardType.MERCH)
                            {
                                FlavorCardObject flavor = (FlavorCardObject)item;
                                if (flavor.cardClass == "Merchandise")
                                {
                                    string temp = Regex.Replace(flavor.name.ToString(), "[^0-9]", string.Empty, RegexOptions.IgnoreCase).ToString();
                                    int k = int.Parse(temp);
                                    WrestlerCardObject w = (WrestlerCardObject)GameSetupItems.instance.wrestlers.cards[k];
                                    item3.gameObject.GetComponent<Text>().text = (GameSetupItems.instance.wrestlers.cards[k].name.ToString() + " 's Merch").ToString();
                                }
                                else
                                {
                                    item3.gameObject.GetComponent<Text>().text = item.name.ToString();
                                }
                                 
                            }
                            else
                            {
                                item3.gameObject.GetComponent<Text>().text = item.name.ToString();
                            }
                            
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        //else if (item3.name == "Rarity")
                        //{
                        //    if (item.myCardType != CardType.VENUE)
                        //    {
                        //        foreach (StoreCard itemY in GameSetupItems.instance.ShopCards)
                        //        {
                        //            if (item.CardNumber == itemY.card.CardNumber)
                        //            {
                        //                if (itemY.card.myCardType == CardType.SHOP_COMMON)
                        //                {
                        //                    item3.gameObject.GetComponent<Text>().text = "Common";
                        //                }
                        //                if (itemY.card.myCardType == CardType.SHOP_RARE)
                        //                {
                        //                    item3.gameObject.GetComponent<Text>().text = "Rare";
                        //                }
                        //                if (itemY.card.myCardType == CardType.SHOP_UNCOMMON)
                        //                {
                        //                    item3.gameObject.GetComponent<Text>().text = "Uncommon";
                        //                }
                        //            }
                        //        }
                        //        if (found == true)
                        //        {
                        //            item3.gameObject.GetComponent<Text>().color = Color.black;
                        //        }
                        //        else if (found == false)
                        //        {
                        //            item3.gameObject.GetComponent<Text>().color = Color.white;
                        //        }
                        //    }
                        //}
                        else if (item3.name == "Type")
                        {
                            item3.gameObject.GetComponent<Text>().text = CardType.ToString();
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.black;
                            }
                            else if (found == false)
                            {
                                item3.gameObject.GetComponent<Text>().color = Color.white;
                            }
                        }
                        else if (item3.name == "CheckBox")
                        {
                            if (found == true)
                            {
                                item3.gameObject.GetComponent<Image>().sprite = CheckedCheckBox;
                            }
                            else if (found == true)
                            {
                                item3.gameObject.GetComponent<Image>().sprite = UnCheckedCheckBox;
                            }
                        }
                    }
                }
                CardNumber++;
            }
        }
        CheckListActiveType.transform.GetChild(0).gameObject.GetComponent<Text>().text = ActiveCheckListType.ToString();
    }


    public void CheckListSwitchType()
    {
        if (ActiveCheckListType == CardType.ALL)
        {
            ActiveCheckListType = CardType.WRESTLER;
        }
        else
        {
            ActiveCheckListType += 1;
            if (ActiveCheckListType == CardType.SHOP_COMMON)
            {
                ActiveCheckListType = CardType.WRESTLER;
            }
            else if (ActiveCheckListType == CardType.FLAVOR)
            {
                ActiveCheckListType = CardType.MIC_SPOT;
            }
        }
        
        PopulateCheckListScreen(ActiveCheckListType);
    }
}
