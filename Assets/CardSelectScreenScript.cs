using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using UnityEngine.EventSystems;

[System.Serializable]
public class NewCardsObject {
	public PopUpMessage message;
	public List<CardObject> cards;

	public NewCardsObject(){
		cards = new List<CardObject>();
	}
}

public class CardSelectScreenScript : MonoBehaviour {
    public enum CardFilter {
        WRESTLERS_ALL,
        VENUES,
        MATCHES,
        FLAVORS,
        MIC_SPOTS,
        SKITS,
        MANAGERS,
        SPONSORS,
        MERCH,
        TAG_TEAMS,
        FEUDS,
        ALL,
        WRESTLERS_GOOD,
        WRESTLERS_BAD,
        WRESTLERS_MAIN_EVENT,
        WRESTLERS_MIDCARDER,
        WRESTLERS_CURTAIN_JERKER
    }

    private bool isSelectingCard;
    public bool isCardSelect;

    public int maximumCards;
    public static CardSelectScreenScript instance;

    public CardFilter currentFilter;
    public Text titleText;
    public Text filterText;
    public Text collectionText;

    public int currentCardIndex;

    public CardView cardViewer;
    public Animator cardViewerScreen;

    public GameObject collectionPanel;
    public GameObject selectButton;
    public GameObject notEnoughString;
    public List<WrestlerCardObject> FoilCards;
    public bool IsAwardShow;

    [HideInInspector]
    public Animator previousScreen;

    [HideInInspector]
    public bool hadHUD;

    private CardSelector selector;

    private GameObject UIManager;
    private GameObject playerHUD;

    GameObject AwardButton;

    private Queue<CardFilter> filterQueue = new Queue<CardFilter>();
    private List<CardObject> cardList = new List<CardObject>();
    private List<CardObject> cardLibrary = new List<CardObject>();

    private string[] alphabet = new string[26]
    {
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z"
    };

    void Awake()
    {
        instance = this;
    }

    public void GenerateFilters(CardFilter newFilter, List<CardType> exclude = null)
    {
        bool isFound;
        currentFilter = newFilter;

        // Wipe queue if it already exists
        if (filterQueue.Count > 0)
            filterQueue.Clear();

        // Set up the queue of filters available
        filterQueue.Enqueue(currentFilter);
        foreach (CardFilter filter in Enum.GetValues(typeof(CardFilter)))
        {
            if (filter == currentFilter)
            {
                continue;
            }

            isFound = false;
            if (exclude != null)
                foreach (CardType f in exclude)
                {
                    if (filter.ToString().Contains(f.ToString()))
                    {
                        isFound = true;
                        break;
                    }
                }

            if (isFound)
                continue;

            filterQueue.Enqueue(filter);
        }

        currentCardIndex = 0;        
    }

    public void OpenCardView(NewCardsObject newCards = null, Animator nextScreen = null, bool showHUD = false) {
        UIManager = GameObject.Find("UIManager");
        playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
        previousScreen = nextScreen;
        hadHUD = showHUD;
        if (playerHUD != null)
            playerHUD.SetActive(false);

        if (newCards != null) {
            cardLibrary = newCards.cards;

            foreach (CardObject card in newCards.cards) {
                if (GamePlaySession.instance.CheckCard(card.CardNumber) == null)
                    GamePlaySession.instance.myCards.Add(card);
            }
            // Now reprocess segment based on new card list
            if (GamePlaySession.instance.currentSegment != null) {
                GamePlaySession.instance.currentSegment.ProcessCardIDs();
                if (nextScreen != null) {
                    SegmentSetup setup = nextScreen.GetComponent<SegmentSetup>();
                    if (setup != null) {
                        setup.SetData(setup.segmentDisplay);
                        setup.UpdateDisplay();
                    }
                }
            }
        } else {
            cardLibrary = GamePlaySession.instance.myCards;
        }

        GenerateFilters(CardFilter.ALL);
        FilterCards();
        UpdateCollectionData();


        SetViewSelect(false);

        if (ScreenHandler.current != null && cardViewerScreen != null) {
            ScreenHandler.current.OpenPanel(cardViewerScreen);
        }
        if (newCards != null && newCards.message != null) {
            if (newCards.message.message != "") {
                PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
                if (popUpWindow != null) {
                    popUpWindow.OpenPopUpWindow(new PopUpMessage[] { newCards.message }, this.cardViewerScreen);
                }
            }
        }
    }

    public void OpenCardSelect(CardSelector caller) {
        // Set the selector and the previous screen
        IsAwardShow = false;
        selector = caller;
        previousScreen = selector.gameObject.GetComponentInParent<Animator>();

        // Refund any previously selected card's costs
        CardObject card = selector.GetCard();

        if (card != null) {
            GamePlaySession.instance.myCash += card.GetPlayCost();
        }

        // Establish and filter the card library
        cardLibrary = caller.cards;


        GenerateFilters(selector.filterType, selector.hideTypes);
        FilterCards();

        if (cardList.Count == 0) {
            return;
        }

        SetViewSelect(true);

        if (ScreenHandler.current != null && cardViewerScreen != null) {
            ScreenHandler.current.OpenPanel(cardViewerScreen);
        }
        else {
            Debug.Log("FAILURE!!!!");
        }
    }

    public void OpenCardSelectAward(GameObject caller)
    {
        //if (ShowObject.instance.showFinished==true)
        //{
        //    Debug.Log("Show Finished...");
        //}
        IsAwardShow = true;
        previousScreen = caller.gameObject.GetComponentInParent<Animator>();
        AwardButton = EventSystem.current.currentSelectedGameObject;
        //if (caller.name=="WrestlerOfTheYear")
        //CardSelector ThisSelector = new CardSelector();
        CardSelector ThisSelector = gameObject.AddComponent<CardSelector>();
        ThisSelector.hideTypes = new List<CardType>();

        switch (caller.name)
        {
            case "WrestlerOfTheYear":
                ThisSelector.filterType = CardFilter.WRESTLERS_ALL;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.WRESTLER)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
            case "TagTeamOfTheYear":
                ThisSelector.filterType = CardFilter.TAG_TEAMS;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.TAG_TEAM)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
            case "MostPopularWrestler":
                ThisSelector.filterType = CardFilter.WRESTLERS_GOOD;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.WRESTLER)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
            case "MostHatedWrestler":
                ThisSelector.filterType = CardFilter.WRESTLERS_BAD;
                foreach (CardType type in Enum.GetValues(typeof(CardType)))
                {
                    if (type != CardType.WRESTLER)
                    {
                        ThisSelector.hideTypes.Add(type);
                    }
                }
                break;
        }
        selector = ThisSelector;

        cardLibrary = GamePlaySession.instance.myCards;

        GenerateFilters(ThisSelector.filterType, ThisSelector.hideTypes);

        FilterCards();

        if (cardList.Count == 0)
        {
            return;
        }

        SetViewSelect(true);

        if (ScreenHandler.current != null && cardViewerScreen != null)
        {
            ScreenHandler.current.OpenPanel(cardViewerScreen);
        }
        else
        {
            Debug.Log("FAILURE!!!!");
        }
    }


    public void SetViewSelect(bool select) {
        isCardSelect = select;

        if (isCardSelect) {
            if (collectionPanel)
                collectionPanel.SetActive(false);
            if (selectButton)
                selectButton.SetActive(true);
            if (filterText && (GamePlaySession.instance.inTutorialMode || selector.filterType != CardFilter.WRESTLERS_ALL))
                filterText.transform.parent.gameObject.SetActive(false);
            else
                filterText.transform.parent.gameObject.SetActive(true);
        } else {
            if (collectionPanel)
                collectionPanel.SetActive(true);
            if (selectButton) {
                selectButton.SetActive(false);
            }
            if (filterText)
                filterText.transform.parent.gameObject.SetActive(true);
        }

        SetScreenTitle();
    }

    public void SetScreenTitle() {
        if (titleText) {
            if (isCardSelect) {
                titleText.text = GetCurrentFilterName();
            } else {
                titleText.text = "Cards";
            }
        }
    }

    public void ReturnToLastScreen() {
        if (hadHUD) {
            playerHUD = GameObject.Find("UIManager").GetComponent<UIManagerScript>().PlayerHUD;

            if (playerHUD != null)
                playerHUD.SetActive(true);
        }

        if (!isCardSelect && previousScreen == null) {
            UIManager = GameObject.Find("UIManager");
            UIManager.GetComponent<UIManagerScript>().GoToCalendar(false);
            return;
        }

        if (isCardSelect) {
            if (selector != null)
                selector.ResetCard();
        }

        if (ScreenHandler.current != null && previousScreen != null) {
            ScreenHandler.current.OpenPanel(previousScreen);
        }
    }
    public void SelectCard(CardView viewer) {
        StartCoroutine(SelectCardC(viewer));
    }
    public IEnumerator SelectCardC(CardView viewer) {
        isSelectingCard = true;
        // Determine card cost
        int cost = viewer.myCard.GetPlayCost();
        // Check we can afford it or if it's free
        if (GamePlaySession.instance.myCash >= cost || cost <= 0) {
            // Subtract card cost
            GamePlaySession.instance.myCash -= cost;

            // Switch screens
            if (ScreenHandler.current != null && previousScreen != null) {
                ScreenHandler.current.OpenPanel(previousScreen);
            }
            else {
                //Debug.Log("FAILURE");
                //Debug.Log(ScreenHandler.current.name);
                //Debug.Log(previousScreen.name);
            }

            // Set the selector to select the card we chose
            yield return StartCoroutine(WaitForSelector(viewer));


        } else {
            yield return StartCoroutine(FlashMessage(notEnoughString));
        }
        isSelectingCard = false;
        yield return null;
    }

    public IEnumerator WaitForSelector(CardView viewer)
    {
        while (selector == null)
        {
            yield return null;
        }
        if (IsAwardShow == false)
        {
            selector.SelectCard(viewer.myCard);
        }
        else if (IsAwardShow == true)
        {
            if (AwardButton != null && AwardButton.GetComponent<Image>()!=null)
            {
                AwardButton.GetComponent<Image>().sprite = viewer.myCard.logoImage;
                AwardButton.transform.GetChild(0).gameObject.GetComponent<Text>().text = "";
                switch (AwardButton.name)
                {
                    case "WrestlerOfTheYearButton":
                        AwardScreenController.instance.WYSelected = true;
                        AwardScreenController.instance.AwardHolders[0] = viewer.myCard;
                        break;
                    case "TagTeamOfTheYearButton":
                        AwardScreenController.instance.TTSelected = true;
                        AwardScreenController.instance.AwardHolders[1] = viewer.myCard;
                        break;
                    case "MostPopularWrestlerButton":
                        AwardScreenController.instance.MPWSelected = true;
                        AwardScreenController.instance.AwardHolders[2] = viewer.myCard;
                        break;
                    case "MostHatedWrestlerButton":
                        AwardScreenController.instance.MHWSelected = true;
                        AwardScreenController.instance.AwardHolders[3] = viewer.myCard;
                        break;
                }
                if (AwardScreenController.instance.MHWSelected && AwardScreenController.instance.MPWSelected && AwardScreenController.instance.WYSelected && AwardScreenController.instance.TTSelected)
                {
                    AwardScreenController.instance.GiveAwardsButton.GetComponent<Button>().interactable = true;
                }
            }
        }
    }
    public void UpdateCollectionData() {
        if (collectionText != null) {
            if (GamePlaySession.instance != null) {
                collectionText.text = "Collection: " + GamePlaySession.instance.myCards.Count.ToString() + "/" + maximumCards.ToString();
                collectionText.transform.parent.gameObject.SetActive(true);
            } else {
                collectionText.transform.parent.gameObject.SetActive(false);
            }
        }
    }

    public void SwitchCardFilter() {
        //if (isCardSelect)
        //	return;

        if (filterQueue.Count > 0) {
            CardFilter lastFilter = filterQueue.Dequeue();
            filterQueue.Enqueue(lastFilter);
            currentFilter = filterQueue.Peek();

            currentCardIndex = 0;
            FilterCards();
        }
    }
    public void OnDisable() {
        if (isSelectingCard)
        {
            if (previousScreen.name != "AwardsGivingScreen")
            {
                //Debug.Log("I AM DISABLING While selecting card");
                UIManager = GameObject.Find("UIManager");
                UIManager.GetComponent<UIManagerScript>().GoToCalendar(false);
            }
        }
    }
    public void FilterCards()
    {
        List<CardObject> filteredCards = new List<CardObject>();
        if (cardLibrary.Count > 0)
        {
            // Determine the current card type to check for, or simply return all cards.
            if (currentFilter == CardFilter.ALL)
            {
                filteredCards = cardLibrary;
            }
            else
            {
                CardType[] filters = CheckFilterType();
                foreach (CardObject card in cardLibrary)
                {
                    foreach (CardType filter in filters)
                    {
                        if (card.myCardType == filter)
                        {
                            if (GamePlaySession.instance.inTutorialMode && card.CardNumber == 406)
                                continue;
                            if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_BAD)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                                if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear)
                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.cardAlignment == "Bad")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        if (wrestler.cardAlignment == "Bad")
                                        {
                                            FoilCards.Add(wrestler);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_GOOD)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                                if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear)
                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.cardAlignment == "Good")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        if (wrestler.cardAlignment == "Good")
                                        {
                                            FoilCards.Add(wrestler);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_MAIN_EVENT)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                                if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear)
                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Main Eventer")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Main Eventer")
                                        {
                                            FoilCards.Add(wrestler);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_MIDCARDER)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                                if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear)
                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Mid Carder")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Mid Carder")
                                        {
                                            FoilCards.Add(wrestler);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_CURTAIN_JERKER)
                            {
                                WrestlerCardObject wrestler = (WrestlerCardObject)card;
                                int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                                if (wrestler.FirstActiveYear <= ThisYear && wrestler.LastActiveYear >= ThisYear)
                                {
                                    if (wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Curtain Jerker")
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                    else if (wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        if (wrestler.characterStatus == "Curtain Jerker")
                                        {
                                            FoilCards.Add(wrestler);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.MERCH)
                            {
                                FlavorCardObject merch = (FlavorCardObject)card;
                                
                                string ParentName="";
                                if (selector.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.name != null)
                                {
                                    ParentName = selector.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.name;
                                }
                                int[] WrestlerCardNumber = { 0, 0, 0, 0, 0 };

                                //Debug.Log("Selector is: " + selector.name);

                                if ((selector.name == "Team1FlavorPanel") && (ParentName != "TagMatchSetupScreen"))
                                {
                                    GameObject WrestlerLogoDisplay = GameObject.Find("Wrestler1Logo");
                                    WrestlerCardNumber[0] = int.Parse(Regex.Replace(WrestlerLogoDisplay.GetComponent<Image>().sprite.name, "_logo", ""));
                                }
                                else if ((selector.name == "Team2FlavorPanel") && (ParentName != "TagMatchSetupScreen"))
                                {
                                    GameObject WrestlerLogoDisplay = GameObject.Find("Wrestler2Logo");
                                    WrestlerCardNumber[0] = int.Parse(Regex.Replace(WrestlerLogoDisplay.GetComponent<Image>().sprite.name, "_logo", ""));
                                }
                                else if ((selector.name == "Team1FlavorPanel") || (selector.name == "Team2FlavorPanel") && (ParentName == "TagMatchSetupScreen"))
                                {
                                    //Debug.Log("TEAM GAMEOBJECT IS: " + selector.transform.parent.gameObject.name);
                                    if (selector.transform.parent.gameObject.name == "Team1Options")
                                    {
                                        GameObject[] WrestlerLogoDisplays = { null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler1Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler2Logo");
                                        for (int i = 0; i < WrestlerLogoDisplays.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_logo", ""));
                                            }
                                        }
                                    }
                                    else if (selector.transform.parent.gameObject.name == "Team2Options")
                                    {
                                        GameObject[] WrestlerLogoDisplays = { null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler3Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler4Logo");
                                        for (int i = 0; i < WrestlerLogoDisplays.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_logo", ""));
                                            }
                                        }
                                    }
                                }

                                else if (selector.name == "FlavorPanel")
                                {
                                    if (selector.transform.parent.gameObject.name == "SkitFlavorOptions")
                                    {
                                        //Debug.Log("Parent is: SkitFlavorOptions");
                                        GameObject[] WrestlerLogoDisplays = { null, null, null, null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler1Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler2Logo");
                                        WrestlerLogoDisplays[2] = GameObject.Find("Wrestler3Logo");
                                        WrestlerLogoDisplays[3] = GameObject.Find("Wrestler4Logo");
                                        WrestlerLogoDisplays[4] = GameObject.Find("Wrestler5Logo");
                                        for (int i = 0; i < WrestlerCardNumber.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_logo", ""));
                                            }
                                        }
                                    }

                                    else if (selector.transform.parent.gameObject.name == "MicSpotFlavorOptions")
                                    {
                                        //Debug.Log("Parent is: SkitFlavorOptions");
                                        GameObject[] WrestlerLogoDisplays = { null, null, null, null, null };
                                        WrestlerLogoDisplays[0] = GameObject.Find("Wrestler1Logo");
                                        WrestlerLogoDisplays[1] = GameObject.Find("Wrestler2Logo");
                                        WrestlerLogoDisplays[2] = GameObject.Find("Wrestler3Logo");
                                        WrestlerLogoDisplays[3] = GameObject.Find("Wrestler4Logo");
                                        WrestlerLogoDisplays[4] = GameObject.Find("Wrestler5Logo");
                                        for (int i = 0; i < WrestlerCardNumber.Length; i++)
                                        {
                                            if (WrestlerLogoDisplays[i] != null)
                                            {
                                                WrestlerCardNumber[i] = int.Parse(Regex.Replace(WrestlerLogoDisplays[i].GetComponent<Image>().sprite.name, "_logo", ""));
                                            }
                                        }
                                    }
                                }


                                foreach (int item in WrestlerCardNumber)
                                {
                                    if (item != 0)
                                    {
                                        if (item == merch.unlockRequirements.MaxOut)
                                        {
                                            filteredCards.Add(card);
                                        }
                                    }
                                }
                            }
                            else if (filter == CardType.WRESTLER && (currentFilter != (CardFilter.WRESTLERS_CURTAIN_JERKER) && (currentFilter != (CardFilter.WRESTLERS_BAD) && (currentFilter != (CardFilter.WRESTLERS_GOOD)) && (currentFilter != (CardFilter.WRESTLERS_MAIN_EVENT) && (currentFilter != (CardFilter.WRESTLERS_MIDCARDER))))))
                            {
                                WrestlerCardObject Wrestler = (WrestlerCardObject)card;
                                int ThisYear = int.Parse(GamePlaySession.instance.myCalendar.years[GamePlaySession.instance.currentYear].yearName);
                                if (Wrestler.FirstActiveYear <= ThisYear && Wrestler.LastActiveYear >= ThisYear)
                                {
                                    if (Wrestler.cardVersion.ToUpper() != "FOIL")
                                    {
                                        filteredCards.Add(card);
                                    }
                                    else if (Wrestler.cardVersion.ToUpper() == "FOIL")
                                    {
                                        FoilCards.Add(Wrestler);
                                    }
                                }
                            }
                            else
                            {
                                filteredCards.Add(card);
                            }
                        }
                    }
                }
            }

            if (filteredCards.Count == 0) {
                // If there's no cards when using this filter, switch to the next one and try again, unless we're in card select mode
                if (!isCardSelect) {
                    SwitchCardFilter();
                    return;
                }
            }
        }
        cardList = filteredCards;
        // Sort the card list based on card number
        cardList.Sort((x, y) => string.Compare(x.name, y.name));

        string filterOutput = "Show: ";
        filterOutput += GetCurrentFilterName();
        filterText.text = filterOutput;

        ShowCurrentCard();
    }

    public string GetCurrentFilterName() {
        string name = "";

        switch (currentFilter) {
            case CardFilter.WRESTLERS_ALL:
                name = "Wrestlers All";
                break;
            case CardFilter.WRESTLERS_BAD:
                name = "Wrestlers Bad";
                break;
            case CardFilter.WRESTLERS_GOOD:
                name = "Wrestlers Good";
                break;
            case CardFilter.WRESTLERS_MAIN_EVENT:
                name = "Wrestlers Main Event";
                break;
            case CardFilter.WRESTLERS_CURTAIN_JERKER:
                name = "Wrestlers Curtain Jerker";
                break;
            case CardFilter.WRESTLERS_MIDCARDER:
                name = "Wrestlers Midcarder";
                break;
            case CardFilter.VENUES:
                name = "Venues";
                break;
            case CardFilter.MATCHES:
                name = "Matches";
                break;
            case CardFilter.FLAVORS:
                name = "Flavors";
                break;
            case CardFilter.MIC_SPOTS:
                name = "Mic Spots";
                break;
            case CardFilter.SKITS:
                name = "Skits";
                break;
            case CardFilter.MANAGERS:
                name = "Managers";
                break;
            case CardFilter.SPONSORS:
                name = "Sponsors";
                break;
            case CardFilter.MERCH:
                name = "Merch";
                break;
            case CardFilter.TAG_TEAMS:
                name = "Tag Teams";
                break;
            case CardFilter.FEUDS:
                name = "Feuds";
                break;
            default:
                name = "All";
                break;
        }

        return name;
    }

    public CardType[] CheckFilterType() {
        switch (currentFilter) {
            case CardFilter.WRESTLERS_ALL:
            case CardFilter.WRESTLERS_BAD:
            case CardFilter.WRESTLERS_CURTAIN_JERKER:
            case CardFilter.WRESTLERS_GOOD:
            case CardFilter.WRESTLERS_MAIN_EVENT:
            case CardFilter.WRESTLERS_MIDCARDER:
                return new CardType[] { CardType.WRESTLER };
            case CardFilter.VENUES:
                return new CardType[] { CardType.VENUE };
            case CardFilter.MATCHES:
                return new CardType[] { CardType.MATCH };
            case CardFilter.FLAVORS:
                return new CardType[] { CardType.FLAVOR, CardType.MANAGER, CardType.SPONSOR, CardType.MERCH, CardType.TAG_TEAM, CardType.FEUD };
            case CardFilter.MIC_SPOTS:
                return new CardType[] { CardType.MIC_SPOT };
            case CardFilter.SKITS:
                return new CardType[] { CardType.SKIT };
            case CardFilter.MANAGERS:
                return new CardType[] { CardType.MANAGER };
            case CardFilter.SPONSORS:
                return new CardType[] { CardType.SPONSOR };
            case CardFilter.MERCH:
                return new CardType[] { CardType.MERCH };
            case CardFilter.TAG_TEAMS:
                return new CardType[] { CardType.TAG_TEAM };
            case CardFilter.FEUDS:
                return new CardType[] { CardType.FEUD };
            default:
                return new CardType[] { CardType.WRESTLER };
        }
    }

    public IEnumerator FlashMessage(GameObject messageString) {
        messageString.SetActive(true);

        yield return new WaitForSeconds(3f);

        messageString.SetActive(false);
    }

    public void ShowCurrentCard() {
        if (cardList.Count > 0)
        {
            if (cardList[currentCardIndex].myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject wrestler = (WrestlerCardObject)cardList[currentCardIndex];
                if (wrestler.cardVersion.ToUpper() != "FOIL")
                {
                    cardViewer.ShowCard(cardList[currentCardIndex]);
                }
            }
            else if (cardList[currentCardIndex].myCardType != CardType.WRESTLER)
            {
                cardViewer.ShowCard(cardList[currentCardIndex]);
            }
        }
    }

    public CardObject GetCurrentCard() {
        return cardList[currentCardIndex];
    }

    public void UserSwipeRight() {
        int modNum = cardList.Count;
        currentCardIndex--;

        if (currentCardIndex < 0) {
            currentCardIndex = modNum - 1;
        } else {
            currentCardIndex = currentCardIndex % modNum;
        }
        ShowCurrentCard();
    }

    public void Slide(Slider value)
    {
        var index = cardList.FindIndex(x => x.name.ToLower().StartsWith(alphabet[(int)value.value]));
        currentCardIndex = index >= 0 ? index : currentCardIndex;
        ShowCurrentCard();
    }

    public void UserSwipeLeft() {
        int modNum = cardList.Count;
        currentCardIndex++;
        currentCardIndex = currentCardIndex % modNum;
        ShowCurrentCard();
    }

    //ALI AMEEN
    public void PromoteWrestlerStatus()
    {
        GameObject managerScript = GameObject.Find("UIManager");
        CardObject card = CardSelectScreenScript.instance.GetCurrentCard();
        WrestlerCardObject W = (WrestlerCardObject)card;
        bool isChamp = false;
        GamePlaySession G = GamePlaySession.instance;
        if ((W.CardNumber == (G.tagChampCardNumbers[0])) || (W.CardNumber == (G.tagChampCardNumbers[1])) || (W.CardNumber == (G.titleChampCardNumber)) || (W.CardNumber == G.worldChampCardNumber))
        {
            isChamp = true;
        }

        if (isChamp == false)
        {
            if (GamePlaySession.instance.myTokens < 1)
            {
                
            }
            
            else if (GamePlaySession.instance.myTokens >= 1)
            {
                UIManager.GetComponent<UIManagerScript>().PlayerHUD.SetActive(true);
                UIManager.GetComponent<UIManagerScript>().PromoteVerifyScreen.gameObject.SetActive(true);
                UIManager.GetComponent<UIManagerScript>().PromoteVerifyScreen.gameObject.transform.SetAsLastSibling();
                GameObject.Find("WrestlerImage").GetComponent<Image>().sprite = card.headshotImage;
            }
        }
        else if (isChamp == true)
        {
            UIManagerScript.instance.CantPromoteChampScreen.gameObject.SetActive(true);
            UIManagerScript.instance.CantPromoteChampScreen.gameObject.transform.SetAsLastSibling();

        }
    }
    public void DemoteWrestlerStatus()
    {
        GameObject managerScript = GameObject.Find("UIManager");
        CardObject card = CardSelectScreenScript.instance.GetCurrentCard();
        WrestlerCardObject W = (WrestlerCardObject)card;
        bool isChamp = false;
        GamePlaySession G = GamePlaySession.instance;
        if ((W.CardNumber == (G.tagChampCardNumbers[0])) || (W.CardNumber == (G.tagChampCardNumbers[1])) || (W.CardNumber == (G.titleChampCardNumber)) || (W.CardNumber == G.worldChampCardNumber))
        {
            isChamp = true;
            //Debug.Log("IS CHAMP");
        }

        if (isChamp == false)
        {
            UIManagerScript.instance.PlayerHUD.SetActive(true);
            UIManagerScript.instance.DemoteVerifyScreen.gameObject.SetActive(true);
            UIManagerScript.instance.DemoteVerifyScreen.gameObject.transform.SetAsLastSibling();
            GameObject.Find("WrestlerImage").GetComponent<Image>().sprite = card.headshotImage;
        }
        else if (isChamp == true)
        {
            UIManagerScript.instance.CantDemoteChampScreen.gameObject.SetActive(true);
            UIManagerScript.instance.CantDemoteChampScreen.gameObject.transform.SetAsLastSibling();
        }
    }
    public void ClosePromoteWrestlerStatus()
    {
        UIManagerScript.instance.PromoteVerifyScreen.gameObject.SetActive(false);
        UIManagerScript.instance.PlayerHUD.SetActive(false);
    }
    public void CloseCantPromoteChamp()
    {
        UIManagerScript.instance.CantPromoteChampScreen.gameObject.SetActive(false);
    }
    public void CloseCantDemoteChamp()
    {
        UIManagerScript.instance.CantDemoteChampScreen.gameObject.SetActive(false);
    }
    public void ConfirmPromoteWrestlerStatus()
    {
        Debug.Log("PROMOTE WRESTLER STATUS CLICKED...");
        WrestlerCardObject card = (WrestlerCardObject)CardSelectScreenScript.instance.GetCurrentCard();

        foreach (CardObject item in GamePlaySession.instance.myCards)
        {
            if (item.myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject W = (WrestlerCardObject)item;
                if (W.CardNumber == card.CardNumber)
                {
                    if (W.pushCurrent < 20)
                    {
                        W.pushCurrent += 3;
                        if (W.pushCurrent >= 20)
                        {
                            W.pushCurrent = 20;
                        }
                    }
                    if (W.characterStatus.ToUpper() == "JOBBER")
                    {
                        W.characterStatus = "Opener";
                    }
                    else if (W.characterStatus.ToUpper() == "OPENER" || W.characterStatus.ToUpper() == "CURTAIN JERKER")
                    {
                        W.characterStatus = "Mid Carder";
                    }
                    else if (W.characterStatus.ToUpper() == "MID CARDER")
                    {
                        W.characterStatus = "Upper Mid Carder";
                    }
                    else if (W.characterStatus.ToUpper() == "UPPER MID CARDER")
                    {
                        W.characterStatus = "Main Eventer";
                    }
                    else if (W.characterStatus.ToUpper() == "MAIN EVENTER")
                    {
                        W.characterStatus = "Legendary";
                    }
                    CardViewBackWrestler.instance.statusData.text = W.characterStatus;
                    CardViewBackWrestler.instance.statPush.text = W.pushCurrent.ToString();
                    //CardViewBackWrestler.instance.PromoteStatusButton.GetComponent<Button>().onClick.AddListener(() => CardSelectScreenScript.instance.PromoteWrestlerStatus());
                    UIManagerScript.instance.PromoteVerifyScreen.gameObject.SetActive(false);
                    GamePlaySession.instance.myTokens -= 1;
                    UIManagerScript.instance.PlayerHUD.GetComponent<HUDScript>().UpdateHUD();
                    
                    UIManagerScript.instance.PlayerHUD.SetActive(false);
                }
            }
        }
    }
    public void ShowFoilCard()
    {
        WrestlerCardObject currentWrestler = (WrestlerCardObject)cardViewer.myCard;
        if (currentWrestler.cardVersion.ToUpper() != "FOIL")
        {
            ShowCurrentCard(cardViewer.myCard, true);
        }
        else if (currentWrestler.cardVersion.ToUpper() == "FOIL")
        {
            ShowCurrentCard(cardViewer.myCard, false);
        }
    }
    public void ShowCurrentCard(CardObject card, bool Foil)
    {
        if (Foil == true)
        {
            if (FoilCards.Count > 0)
            {
                foreach (CardObject item in FoilCards)
                {
                    if (item.CardNumber == card.CardNumber + 1)
                    {
                        cardViewer.ShowCard(item);
                    }
                }
            }
        }
        else if (Foil == false)
        {
            if (cardList.Count > 0)
            {
                foreach (CardObject item in cardList)
                {
                    if (item.CardNumber == card.CardNumber - 1)
                    {
                        cardViewer.ShowCard(item);
                    }
                }
            }
        }
    }
    public void CloseDemoteWrestlerStatus()
    {
        UIManagerScript.instance.DemoteVerifyScreen.gameObject.SetActive(false);
        UIManagerScript.instance.PlayerHUD.SetActive(false);
    }
    public void ConfirmDemoteWrestlerStatus()
    {
        WrestlerCardObject card = (WrestlerCardObject)CardSelectScreenScript.instance.GetCurrentCard();

        foreach (CardObject item in GamePlaySession.instance.myCards)
        {
            if (item.myCardType == CardType.WRESTLER)
            {
                WrestlerCardObject W = (WrestlerCardObject)item;
                //Debug.Log(W.name + W.characterStatus);
                if (W.name == card.name)
                {
                    //Debug.Log("CARD FOUND: " + item.name);
                    if (W.pushCurrent > 1)
                    {
                        W.pushCurrent -= 3;
                        if (W.pushCurrent <= 1)
                        {
                            W.pushCurrent = 1;
                        }
                    }
                    if (W.characterStatus.ToUpper() == "OPENER")
                    {
                        W.characterStatus = "Jobber";
                    }
                    else if (W.characterStatus.ToUpper() == "MID CARDER")
                    {
                        W.characterStatus = "Curtain Jerker";
                    }
                    else if (W.characterStatus.ToUpper() == "UPPER MID CARDER")
                    {
                        W.characterStatus = "Mid Carder";
                    }
                    else if (W.characterStatus.ToUpper() == "MAIN EVENTER")
                    {
                        W.characterStatus = "Upper Mid Carder";
                    }
                    else if (W.characterStatus.ToUpper() == "LEGENDARY")
                    {
                        W.characterStatus = "Main Eventer";
                    }
                    CardViewBackWrestler.instance.statusData.text = W.characterStatus;
                }
            }
        }

        int newPush = (int.Parse(CardViewBackWrestler.instance.statPush.text.ToString())) - 3;
        if (newPush <= 1)
        {
            newPush = 1;
        }
        CardViewBackWrestler.instance.statPush.text = newPush.ToString();
        UIManagerScript.instance.DemoteVerifyScreen.gameObject.SetActive(false);
        
        UIManagerScript.instance.PlayerHUD.GetComponent<HUDScript>().UpdateHUD();

        UIManagerScript.instance.PlayerHUD.SetActive(false);


    }

}
